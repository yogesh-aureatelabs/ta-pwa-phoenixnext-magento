<?php

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';

$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

/** @var \Magento\Framework\App\ResourceConnection $connection */
$resource = $obj->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();

$attSql = "SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 1 AND attribute_code = 'customer_telephone'";
$attResult = $connection->fetchRow($attSql);

$table = 'customer_address_entity';
$selectSql = "SELECT entity_id, parent_id, telephone FROM $table WHERE `telephone` REGEXP '[^a-zA-Z0-9]'";
$result = $connection->fetchAll($selectSql);

// use the connection here
foreach ($result as $res) {
    // Remove special characters
    $updatedValue = preg_replace('/[^,\/0-9\-]/', '', $res['telephone']);

    // Replace +66 to 0
    $updatedValue = str_replace(['+66', '-'], ['0', ''], $updatedValue);

    $newTelephone = [];
    if (strpos($updatedValue, ',') !== false) {
        $telePhone = splitTwoNumbers($updatedValue, ',');
        $updatedValue = $telePhone[0] ?? null;
        if (isset($telePhone[1])) {
            $newTelephone[] = [
                'attribute_id' => $attResult['attribute_id'],
                'entity_id' => $res['parent_id'],
                'value' => $telePhone[1]
            ];
        }
    }

    if (strpos($updatedValue, '/') !== false) {
        // $telePhone = splitTwoNumbers($updatedValue, ',');
        $telePhone = splitTwoNumbers($updatedValue, '/');
        $updatedValue = $telePhone[0] ?? null;
    }

    $data = ['telephone' => $updatedValue];
    $where = ['entity_id=?' => $res['entity_id']];
    try {
        $connection->update($table, $data, $where);    
    } catch (\Exception $e) {
        throw new \Exception($e->getMessage());
    }

    if (!empty($newTelephone)) {
      try {
        $connection->insertOnDuplicate('customer_entity_varchar', $newTelephone);
      } catch (\Exception $e) {
        throw new \Exception($e->getMessage());
      }
    }
}

function splitTwoNumbers($number, $seperator) {
    $telephones = explode($seperator, $number);
    $telephones = array_map('trim', $telephones);
    return str_replace(['+66', '-'], ['0', ''], $telephones);
}
