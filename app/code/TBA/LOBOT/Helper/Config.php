<?php
namespace TBA\LOBOT\Helper;

use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 */
class Config extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Const XML path
     */
    const XML_PATH_KM_NAME = 'lobot_cron/profile/name';
    const XML_PATH_KM_ADD1 = 'lobot_cron/profile/adress1';
    const XML_PATH_KM_ADD2 = 'lobot_cron/profile/adress2';
    const XML_PATH_KM_ADD3 = 'lobot_cron/profile/adress3';
    const XML_PATH_KM_TAXD = 'lobot_cron/profile/taxid';

    /**
     * Get company name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_KM_NAME,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get company address 1
     * 
     * @return string
     */
    public function getAddress1()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_KM_ADD1,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get company address 1
     * 
     * @return string
     */
    public function getAddress2()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_KM_ADD2,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get company address 1
     * 
     * @return string
     */
    public function getAddress3()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_KM_ADD3,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get company TAX Id
     * 
     * @return string
     */
    public function getTaxId()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_KM_TAXD,
            ScopeInterface::SCOPE_STORE
        );
    }

}