<?php

namespace TBA\LOBOT\Block\Adminhtml\Order\View;

class ViewTrackingNumber extends \Magento\Framework\View\Element\Template
{
	private $connection;

	protected $timezone;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\App\ResourceConnection $connection,
		\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
	) {
		parent::__construct($context);
		$this->connection = $connection;
		$this->timezone = $timezone;
	}

	public function getTrackingNumber()
	{
		$orderId = $this->getRequest()->getParam('order_id');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderId);

		return $order->getIncrementId();
	}

	public function getShippingDate()
	{
		$orderId = $this->getRequest()->getParam('order_id');
		$connection = $this->connection->getConnection();
		$table = $this->connection->getTableName('sales_shipment');

		$sql = "SELECT MAX(`created_at`) AS date FROM $table WHERE `order_id` = $orderId";
		$date = $connection->fetchOne($sql);
		if (!empty($date)) {
			return $this->timezone->date($date)->format('M d, Y h:i A');
		}
		return '';
	}
}
