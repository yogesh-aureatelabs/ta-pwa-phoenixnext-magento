<?php

namespace TBA\LOBOT\Controller\LobotHelper;

use \Psr\Log\LoggerInterface;

class LobotSetOrderTrackingNumber
{
    protected $logger;
    protected $orderFactory;

    /**
     * @var \Magento\Sales\Model\Convert\Order
     */
    protected $_convertOrder;

    /**
     * @var \Magento\Shipping\Model\ShipmentNotifier
     */
    protected $_shipmentNotifier;

    public function __construct(
        LoggerInterface $logger,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Convert\Order $convertOrder,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier
    ) {
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
        $this->_convertOrder = $convertOrder;
        $this->_shipmentNotifier = $shipmentNotifier;
    }

    public function UpdateTrackingNumberByOrderId($shipedOrderList)
    {
        foreach ($shipedOrderList as $orderId => $dataBean) {
            $trackingNumber = $dataBean['trackingNumber'];
            $shippingDate = $dataBean['shippingDate'];
            $order = $this->orderFactory->create()->loadByIncrementId($orderId);
            if ($order->getId()) {
                // to check order can ship or not
                if ($order->canShip()) {
                    $orderShipment = $this->_convertOrder->toShipment($order);
                    foreach ($order->getAllItems() as $orderItem) {
                        // Check virtual item and item Quantity
                        if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                            continue;
                        }
                        $qty = $orderItem->getQtyToShip();
                        $shipmentItem = $this->_convertOrder->itemToShipmentItem($orderItem)->setQty($qty);
                        $orderShipment->addItem($shipmentItem);
                    }
                    $orderShipment->register();
                    $orderShipment->getOrder()->setIsInProcess(true);
                    // Save created Order Shipment
                    $orderShipment->save();
                    // $orderShipment->getOrder()->save();
                    // Send Shipment Email
                    $this->_shipmentNotifier->notify($orderShipment);
                    $orderShipment->save();
                }

                $order->setData('trackingNumber', $trackingNumber);
                $order->setData('shippingDate', $shippingDate);
                $order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE);
                $order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
                $order->save();

                $this->logger->info("LobotSetOrderTrackingNumber complete Order : " . $orderId . " : with TN : " . $order->getData('trackingNumber'));
                $this->logger->info("LobotSetOrderTrackingNumber complete Order : " . $orderId . " : with SD : " . $order->getData('shippingDate'));
            } else {
                $this->logger->info("LobotSetOrderTrackingNumber Not Found Order : " . $orderId);
            }
        }
    }
}
