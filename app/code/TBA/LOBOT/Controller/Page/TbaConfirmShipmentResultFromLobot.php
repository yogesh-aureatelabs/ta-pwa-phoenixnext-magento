<?php
namespace TBA\LOBOT\Controller\Page;
use \TBA\LOBOT\Controller\LobotHelper\LobotSetOrderTrackingNumber;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;

class TbaConfirmShipmentResultFromLobot extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $lobotSetOrderTrackingNumber;
    protected $lobotSftpHelper;
    protected $logger;

    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LobotSftpHelper $lobotSftpHelper,
        LobotSetOrderTrackingNumber $lobotSetOrderTrackingNumber)
    {
       $this->resultJsonFactory = $resultJsonFactory;

       parent::__construct($context);
       $this->logger = $logger;
       $this->lobotSetOrderTrackingNumber = $lobotSetOrderTrackingNumber;
       $this->lobotSftpHelper = $lobotSftpHelper;
    }

    public function execute()
    {
      $result = $this->resultJsonFactory->create();
      $data = ['message' => 'Hello world!'];
      echo 'TbaConfirmShipmentResultFromLobot';
      //$iFlist = $this->GetLobotShippingResultLocalFile();
      $iFlist = $this->lobotSftpHelper->GetLobotShipmentResult();
      $productTargetInfo = array();
      $dir = '/var/www/www.phoenixnext.com/tmpLobotInterface/';

      foreach ($iFlist as $filename) 
      {

        //$myfile = fopen("/tmpLobotInterface/" . $filename, "r") or die("Unable to open file!");
        $myfile = fopen($dir . $filename, "r") or die("Unable to open file!");
        // Output one line until end-of-file
        while(!feof($myfile)) {
          $content = fgets($myfile);
          echo $content . "\n";
          if($content == "")
            continue;
          $bean = $this->ShippingResultDataParser($content);
          $targetOrder = trim($bean['orderId']," ");
          $dataBean  = array(
          'trackingNumber'=>trim($bean['trackingNumber']," "),
          'shippingDate'=>trim($bean['shippingDate']," "),
          );

          $productTargetInfo[$targetOrder] = $dataBean;
        }
        fclose($myfile);
      }

      $this->lobotSetOrderTrackingNumber->UpdateTrackingNumberByOrderId($productTargetInfo);
      return $result->setData($data);
    }

    private function GetLobotShippingResultLocalFile()
    {
      $filesNameList = array();
      $dir = '/var/www/www.phoenixnext.com/tmpLobotInterface/';
      if ($handle = opendir($dir)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
            }
        }
        closedir($handle);
      }

      $glob_key = $dir . 'BHKSKJ*';

      $loggerMessage = "Finding files : " . $glob_key;
      $this->logger->info($loggerMessage);
      echo $loggerMessage . "\n";

      foreach (glob($glob_key) as $filename) 
      {
          $loggerMessage = "Found file " . basename($filename);
          $this->logger->info($loggerMessage);
          echo $loggerMessage . "\n";
          array_push($filesNameList,basename($filename));
      }

      return $filesNameList;
    }

    private function ShippingResultDataParser($content)
    {
      $orderId = substr($content,0,10);
      $trackingNumber = substr($content,126,20);
      $shippingDate = substr($content,110,8);
      $bean  = array(
          'orderId'=>$orderId,
          'trackingNumber'=>$trackingNumber,
          'shippingDate'=>$shippingDate,
      );

      return $bean;
    }
}