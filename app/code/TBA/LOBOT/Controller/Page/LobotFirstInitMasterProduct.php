<?php
namespace TBA\LOBOT\Controller\Page;
use \TBA\LOBOT\Controller\LobotHelper\LobotProductInitTsvBuilder;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;

class LobotFirstInitMasterProduct extends \Magento\Framework\App\Action\Action
{
	protected $lobotProductInitTsvBuilder;

  protected $lobotSftpHelper;
	protected $resultJsonFactory;
    protected $productCollectionFactory;
	
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
       LobotProductInitTsvBuilder $lobotProductInitTsvBuilder,
       LobotSftpHelper $lobotSftpHelper,
       \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
	{
	     $this->resultJsonFactory = $resultJsonFactory;
       $this->productCollectionFactory = $productCollectionFactory;
       $this->lobotProductInitTsvBuilder = $lobotProductInitTsvBuilder;
       $this->lobotSftpHelper = $lobotSftpHelper;
	   
	     parent::__construct($context);
	}

    public function execute()
    {
    	//$collection = $this->productCollectionFactory->create();
      //$collection->addAttributeToSelect('*');
            //->addAttributeToSelect('*')->where('stock_status_index.stock_status IN(1,0)')
            //->load();

      $collection = $this->productCollectionFactory->create()
                    ->addAttributeToSelect('*')
                    ->load();
      //Fix : Disabled product not coming in product collection in ver-Mage2.2.2 
      /*$collection->clear();

      $fromAndJoin = $collection->getSelect()->getPart('FROM');
      foreach ($fromAndJoin as $key => $index) {
        if ($key == 'stock_status_index') {
          $index['joinType'] = 'left join';
        }
        $updatedfromAndJoin[$key] = $index;
      }
      $collection->getSelect()->setPart('FROM', $updatedfromAndJoin);

      $where = $collection->getSelect()->getPart('where');
      foreach ($where as $key => $condition) {
         if(strpos($condition, 'stock_status_index.stock_status = 1') !== false){
                 $updatedWhere[] = '(stock_status_index.stock_status IN (1,0))';
          } else {
                  $updatedWhere[] = $condition;
           }  
      }
      $collection->getSelect()->setPart('where', $updatedWhere);*/

      $collection->load();

      echo 'LobotFirstInitMasterProduct : ' . count($collection);
      echo (string) $collection->getSelect();  
		  $filePath = $this->lobotProductInitTsvBuilder->ProductToLobotTsvBuilder($collection);
      $this->lobotSftpHelper->PassFileToLobot($filePath);
    }
}