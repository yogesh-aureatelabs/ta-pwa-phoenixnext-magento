<?php 
namespace Rokanthemes\CategoryProductTab\Block;
use \Psr\Log\LoggerInterface;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject\IdentityInterface;

class GetCategoryProductTabBlock extends \Magento\Catalog\Block\Product\AbstractProduct
{
    protected $_defaultToolbarBlock = 'Magento\Catalog\Block\Product\ProductList\Toolbar';

    protected $_postDataHelper;
    protected $_catalogLayer;
    protected $_productCollection;

	protected $logger;
    protected $categoryFactory;

    protected $categoryRepository;
    protected $productCollectionFactory;
    protected $storeManager;
    protected $catalogConfig;
    protected $productVisibility;
    protected $scopeConfig;

    protected $urlHelper;

    public function __construct(LoggerInterface $logger,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        array $data = []
       ) 
    {
        $this->logger = $logger;
        $this->categoryFactory = $categoryFactory;

        $this->_catalogLayer = $layerResolver->get();
        $this->_postDataHelper = $postDataHelper;

        $this->categoryRepository = $categoryRepository;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $context->getStoreManager();
        $this->catalogConfig = $context->getCatalogConfig();
        $this->productVisibility = $productVisibility;

        $this->urlHelper = $urlHelper;

        parent::__construct(
            $context,
            $data
        );
    }

    public function getProducts()
    {
        $storeId   = $this->storeManager->getStore()->getId();
        $products  = $this->productCollectionFactory->create()->setStoreId($storeId);
        $todayDate = date('Y-m-d', time());

        /*$products
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite()
            ->setVisibility($this->productVisibility->getVisibleInCatalogIds())
            ->addAttributeToFilter('news_from_date', array('date'=>true, 'to'=> $todayDate))
            ->addAttributeToFilter('news_to_date', array('date'=>true, 'from'=> $todayDate))
            ->addAttributeToSort('news_from_date','desc');  
        $products->setPageSize($this->getConfig('qty'))->setCurPage(1);*/

        $productsCol = $this
        ->getCategory(3)
        ->getProductCollection()
        ->addAttributeToSelect('*')
        ->setPageSize(3);

        $productsCol->getSelect()->orderRand();

        $products = $productsCol->getItems();
        /*$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );*/

        $this->logger->info("Rokanthemes\CategoryProductTab get product");
        $this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );
        return $products;
    }
    public function getConfig($att) 
    {
        //$path = 'newproduct/newproduct_config/' . $att;
        //return $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return false;
    }
    
    function cut_string_featuredproduct($string,$number){
        if(strlen($string) <= $number) {
            return $string;
        }
        else {  
            if(strpos($string," ",$number) > $number){
                $new_space = strpos($string," ",$number);
                $new_string = substr($string,0,$new_space)."..";
                return $new_string;
            }
            $new_string = substr($string,0,$number)."..";
            return $new_string;
        }
    }
    
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }

    /*public function getProducts($id)
    {
        $productsCol = $this
        ->getCategory($id)
        ->getProductCollection()
        ->addAttributeToSelect('*')
        ->setPageSize(3);

        $productsCol->getSelect()->orderRand();

        $products = $productsCol->getItems();
        /*$this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $products]
        );*

        $this->logger->info("Rokanthemes\CategoryProductTab get product");

        return $products;
    }*/

    public function getCategory($id)
    {
        $category = $this->categoryFactory->create()->load($id);
        return $category;
    }
}