<?php

namespace Aureatelabs\NostoApi\Model\Api;

use Aureatelabs\NostoApi\Api\ApiInterface;
use Magento\Store\Model\StoreManager;
use Psr\Log\LoggerInterface;

/**
 * Class Bestseller
 */
class Bestseller implements ApiInterface
{
    protected $logger;
    protected $resource;
    protected $productRepository;
    protected $categoryCollection;
    protected $_reviewFactory;
    protected $_ratingFactory;
    protected $_storeManager;
    protected $stockRegistry;
    protected $priceHelper;
    protected $currency;

    /**
     * __construct
     *
     * @param  LoggerInterface $logger
     * @param  \Magento\Framework\App\ResourceConnection $connection
     * @return void
     */
    public function __construct(
        LoggerInterface $logger,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\Rating $ratingFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        StoreManager $storeManager,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Directory\Model\Currency $currency
    ) {
        $this->logger = $logger;
        $this->resource = $resource;
        $this->productRepository = $productRepository;
        $this->categoryCollection = $categoryCollection;
        $this->_reviewFactory = $reviewFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_storeManager = $storeManager;
        $this->stockRegistry = $stockRegistry;
        $this->priceHelper = $priceHelper;
        $this->currency = $currency;
    }

    /**
     * @inheritdoc
     */
    public function getLast30Days()
    {
        $response = ['success' => false];

        try {
            $result = $this->getResult(30);
            $response = ['success' => true, 'message' => 'data found.', 'data' => $result];
        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            $this->logger->info($e->getMessage());
        }

        return json_encode($response);
    }

    /**
     * @inheritdoc
     */
    public function getLast45Days()
    {
        $response = ['success' => false];

        try {
            $result = $this->getResult(45);
            $response = ['success' => true, 'message' => 'data found.', 'data' => $result];
        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            $this->logger->info($e->getMessage());
        }

        return json_encode($response);
    }

    /**
     * getResult
     */
    public function getResult($days)
    {
        $connection = $this->resource->getConnection();
        $sku = $days == 30 ? 'LN' : 'MG';
        $days = 30;
        $selectAttribute = $connection->select(['entity_id'])->from($this->resource->getTableName('eav_attribute'))
        ->where('attribute_code = ?','status')
        ->where('entity_type_id = ?', 4);
        $statusAttributeId = $connection->fetchOne($selectAttribute);

        $select = $connection->select()
            ->from(['soi' => $this->resource->getTableName('sales_order_item')])
            ->joinLeft(
                ['cpei' => $connection->getTableName('catalog_product_entity_int')],
                'soi.product_id = cpei.entity_id',
                ['cpei.entity_id as Catalog Product Entity ID']
            )->joinLeft(
                ['csi' => $connection->getTableName('cataloginventory_stock_item')],
                'soi.product_id = csi.product_id',
                ['*']
            )
            ->where('soi.created_at >= ?', new \Zend_Db_Expr('DATE_ADD(CURRENT_TIMESTAMP() , INTERVAL -' . $days . ' DAY)'))
            ->where('sku LIKE ?', '%' . $sku . '%')
            ->where('cpei.value =  ?', 1)
            ->where('csi.is_in_stock =  ?', 1)
            ->where('cpei.attribute_id =  ?', ($statusAttributeId)? $statusAttributeId: 97)
            ->group('sku')
            ->order('SUM(qty_invoiced) DESC')
            ->limit(10);

        $resultSet =  $connection->fetchAll($select);
        $result = [];
        foreach ($resultSet as $record) {
            $product = $this->productRepository->getById($record['product_id']);
            $categoryIds = $product->getCategoryIds();

            $_ratingSummary = $this->_ratingFactory->getEntitySummary($record['product_id']);
            $ratingCollection = $this->_reviewFactory->create()->getResourceCollection()->addStoreFilter(
                $this->_storeManager->getStore()->getId()
            )->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)->addEntityFilter('product', $record['product_id']);
            $review_count = count($ratingCollection); // How many review in that specific product

            $stockItem = $this->stockRegistry->getStockItem($record['product_id']);
            $isInStock = $stockItem ? $stockItem->getIsInStock() : false;

            $product_rating = 0;
            if ($_ratingSummary->getCount() > 0) {
                $product_rating = $_ratingSummary->getSum() / $_ratingSummary->getCount();
            }

            $categories = $this->categoryCollection->create()
                ->addAttributeToSelect('*')
                ->addIdFilter($categoryIds);

            $productCategories = [];
            foreach ($categories as $category) {
                $productCategories[] = $category->getName();
            }

            $formattedPrice = $this->priceHelper->currency($record['price'], true, false);
            $productUrl = explode('/', $product->getProductUrl());
            $pUrl = end($productUrl);
            $result[] = [
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'url' => $pUrl,
                'categories' => $productCategories,
                'review_count' => $review_count,
                'product_id' => $product->getId(),
                'sku' => $product->getSku(),
                'price' => $product->getPrice(),
                'special_price' => $product->getFinalPrice(),
                'price_currency_code' => $this->_storeManager->getStore()->getBaseCurrencyCode(),
                'list_price' => $product->getPrice(),
                'price_text' => $formattedPrice,
                'image' => $product->getData('image'),
                'image_url' => $product->getData('image'),
                'skus' => [],
                'tags1' => [],
                'tags2' => [],
                'tags3' => [],
                'custom_fields' => [
                    "stock_status" => $isInStock,
                    "barcode" => $product->getData('barcode'),
                ],
                'alternate_image_urls' => [
                    $product->getData('thumbnail'),
                    $product->getData('small_image'),
                ],
                'rating_value' => $product_rating,
                'list_price_text' => $formattedPrice,
            ];
        }

        return $result;
    }
}
