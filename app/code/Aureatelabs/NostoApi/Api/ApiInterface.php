<?php

namespace Aureatelabs\NostoApi\Api;

/**
 * interface ApiInterface 
 */
interface ApiInterface
{
    /**
     * GET for get api
     * @return string
     */
    public function getLast30Days();

    /**
     * GET for get api
     * @return string
     */
    public function getLast45Days();
}
