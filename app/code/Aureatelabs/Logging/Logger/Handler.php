<?php

namespace Aureatelabs\Logging\Logger;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;
use Magento\Framework\Filesystem\DriverInterface;

/**
 * Class Handler
 */
class Handler extends Base
{
    protected $loggerType = Logger::INFO;
    protected $fileName;

    public function __construct(DriverInterface $filesystem)
    {
        $this->fileName = "var/log/api/custom_logging_" . date('Ymd') . ".log";
        parent::__construct($filesystem);
    }
}
