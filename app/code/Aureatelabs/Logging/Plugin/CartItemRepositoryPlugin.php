<?php

namespace Aureatelabs\Logging\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Aureatelabs\Logging\Logger\Logger;
use Magento\Checkout\Model\CartFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartItemInterface;

/**
 * Class CartItemRepositoryPlugin
 */
class CartItemRepositoryPlugin
{
    protected $_productRepositoryInterface;
    protected $_cartItemRepositoryInterface;
    protected $_cartFactory;
    protected $quoteRepository;
    protected $logger;

    /**
     * Plugin constructor.
     *
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param CartItemRepositoryInterface $cartItemRepositoryInterface
     * @param CartFactory $cartFactory
     * @param CartRepositoryInterface $quoteRepository
     * @param Logger $logger
     */
    public function __construct(
        ProductRepositoryInterface $productRepositoryInterface,
        CartItemRepositoryInterface $cartItemRepositoryInterface,
        CartFactory $cartFactory,
        CartRepositoryInterface $quoteRepository,
        Logger $logger
    ) {
        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_cartItemRepositoryInterface = $cartItemRepositoryInterface;
        $this->_cartFactory = $cartFactory;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
    }

    /**
     * beforeSave
     *
     * @param  CartItemRepositoryInterface $subject
     * @param  CartItemInterface $cartItem
     * @return void
     */
    public function beforeSave(CartItemRepositoryInterface $subject, CartItemInterface $cartItem)
    {
        $cartId = $cartItem->getQuoteId();
        $sku = $cartItem->getSku();
        $quote = $this->quoteRepository->getActive($cartId);
        $customerId = $quote->getCustomerId();

        $items['customer_id'] = !is_null($customerId) ? $customerId : '';
        $items['quote_item_id'] = $cartId;
        $items['sku'] = $sku;
        $items['requested_at'] = date('Y-m-d H:i:s');

        $this->logger->log('INFO','Before AddToCart: ' . json_encode($items));
        $file = '/var/www/www.phoenixnext.com/var/log/api/custom_logging_'.date('Ymd').'.log';
        file_put_contents($file, 'Before AddToCart: ' . json_encode($items) . "\n", FILE_APPEND);
    }

    /**
     * afterSave
     *
     * @param  CartItemRepositoryInterface $subject
     * @param  mixed $result
     * @param  CartItemInterface $cartItem
     */
    public function afterSave(CartItemRepositoryInterface $subject, $result, CartItemInterface $cartItem)
    {
        $cartId = $cartItem->getQuoteId();
        $quote = $this->quoteRepository->getActive($cartId);
        $quoteItems = $quote->getItems();
        $customerId = $quote->getCustomerId();

        $items['customer_id'] = !is_null($customerId) ? $customerId : '';
        $items['response_status_code'] = http_response_code();
        $items['quote_item_id'] = $cartId;
        $items['responded_at'] = date('Y-m-d H:i:s');
        $items['total_items'] = $quote->getItemsCount();

        foreach ($quoteItems as $quoteItem) {
            $items[$cartId][] = [
                'item_id' => $quoteItem->getItemId(),
                'sku' => $quoteItem->getSku(),
                'qty' => $quoteItem->getQty()
            ];
        }

        $this->logger->log('INFO','After AddToCart: ' . json_encode($items));
        $file = '/var/www/www.phoenixnext.com/var/log/api/custom_logging_'.date('Ymd').'.log';
        file_put_contents($file, 'After AddToCart: ' . json_encode($items) . "\n", FILE_APPEND);

        return $result;
    }
}
