<?php

namespace Aureatelabs\Logging\Plugin;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Aureatelabs\Logging\Logger\Logger;

/**
 * Class OrderPlugin
 */
class OrderPlugin
{
    protected $orderExtensionFactory;
    protected $logger;

    /**
     * Plugin constructor.
     *
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(
        OrderExtensionFactory $orderExtensionFactory,
        Logger $logger
    ) {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->logger = $logger;
    }

    /**
     * beforeGetList
     *
     * @param  OrderRepositoryInterface $subject
     * @param  Collection $resultOrder
     */
    public function beforeGetList(OrderRepositoryInterface $subject, \Magento\Framework\Api\SearchCriteria $crieteria)
    {
        $items = [];
        $filterGroups = $crieteria->getFilterGroups();
        foreach ($filterGroups as $groups) {
            foreach ($groups->getFilters() as $filter) {
                $items['customer_id'] = $filter->getField() == 'customer_id' ? $filter->getValue() : '';
            }
        }

        $items['requested_at'] = date('Y-m-d H:i:s');
        $this->logger->log('INFO','Before OrderList: ' . json_encode($items));
        $file = '/var/www/www.phoenixnext.com/var/log/api/custom_logging_'.date('Ymd').'.log';
        file_put_contents($file, 'Before OrderList: ' . json_encode($items) . "\n", FILE_APPEND);
    }

    /**
     * afterGetList
     *
     * @param  OrderRepositoryInterface $subject
     * @param  Collection $resultOrder
     */
    public function afterGetList(OrderRepositoryInterface $subject, Collection $resultOrder)
    {
        $items = [];
        $orders = $resultOrder->getItems();

        foreach ($orders as $order) {
            $items['customer_id'] = $order->getCustomerId();
        }

        $items['response_status_code'] = http_response_code();
        $items['count_orders'] = $resultOrder->getTotalCount();
        $items['responded_at'] = date('Y-m-d H:i:s');

        $this->logger->log('INFO','After OrderList: ' . json_encode($items));
        $file = '/var/www/www.phoenixnext.com/var/log/api/custom_logging_'.date('Ymd').'.log';
        file_put_contents($file, 'After OrderList: ' . json_encode($items) . "\n", FILE_APPEND);

        return $resultOrder;
    }
}
