<?php

namespace Aureatelabs\ReviewbyCustomer\Plugin;

use Magento\Indexer\Model\IndexerFactory;

class ReviewRepository
{
    /**
     * @var IndexerFactory
     */
    private $indexFactory;

    /**
     * @param IndexerFactory $indexFactory
     */
    public function __construct(
        IndexerFactory $indexFactory
    ) {
        $this->indexFactory = $indexFactory;
    }

    public function afterSave(
        \Divante\ReviewApi\Model\ReviewRepository $subject,
        $result
    ) {

        $index = 'vsbridge_review_indexer';

        $indexFactory = $this->indexFactory->create()->load($index);
        $indexFactory->reindexAll($index);
        $indexFactory->reindexRow($index);

		return $result;
	}
}