<?php
/**
 * Copyright © 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\ReviewbyCustomer\Api;

interface ReviewsManagementInterface
{

    /**
     * GET for reviews api
     * @param string $customer_id
     * @return string
     */
    public function getReviews($customer_id);
}

