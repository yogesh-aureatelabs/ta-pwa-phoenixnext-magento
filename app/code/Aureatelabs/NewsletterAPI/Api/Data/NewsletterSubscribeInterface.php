<?php
namespace Aureatelabs\NewsletterAPI\Api\Data;

/**
 * NewsletterSubscribeInterface
 */
interface NewsletterSubscribeInterface
{
    /**
     * Get success
     *
     * @return boolean
     */
    public function getSuccess();

    /**
     * Get message
     *
     * @return string|null
     */
    public function getMessage();
}
