<?php

namespace Aureatelabs\OrderExport\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface OrderExportSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get orderExport list.
     *
     * @return OrderExportInterface[]
     */
    public function getItems();

    /**
     * Set orderExport list.
     *
     * @param OrderExportInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
