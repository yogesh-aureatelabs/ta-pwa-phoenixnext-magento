<?php

namespace Aureatelabs\OrderExport\Model\OrderExport;

use Magento\Framework\Serialize\Serializer\Json;

class Consumer
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var \TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder
     */
    private $lobotOrderTsvBuilder;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * Consumer constructor.
     * @param Json $json
     * @param \Psr\Log\LoggerInterface $logger
     * @param \TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder $lobotOrderTsvBuilder
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     */
    public function __construct(
        Json $json,
        \Psr\Log\LoggerInterface $logger,
        \TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder $lobotOrderTsvBuilder,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->logger = $logger;
        $this->json = $json;
        $this->lobotOrderTsvBuilder = $lobotOrderTsvBuilder;
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * @param $orders
     */
    public function process($orders)
    {
        try {
            $orderIds = $this->json->unserialize($orders);
            $this->execute($orderIds);
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param $orderIds
     */
    private function execute($orderIds)
    {
        $orderCollection = $this->orderCollectionFactory->create()->addFieldToFilter('entity_id', ['in' => $orderIds]);
        if ($orderCollection->getSize()) {
            foreach ($orderCollection as $order) {
                $this->AssignDataBean($order);
            }
        }
    }

    /**
     * @param $order
     */
    private function AssignDataBean($order)
    {
        $orderId = $order->getId();
        $status = $order->getStatus();

        $order->setData('trackingNumber', "");
        $order->setData('shippingDate', "");
        $order->setStatus("Order_Received");
        $order->save();

        $this->logger->info('SetOrderToLobotAndResetStatus Order ID is :: ' . $orderId);
        $this->logger->info('SetOrderToLobotAndResetStatus Order Status is :: ' . $status);

        $taxId = "Tax ID : 0105559143340";
        $kadokawaName = "KADOKAWA AMARIN COMPANY LIMITED";
        $kadokawaAdress1 = "378 Chaiyaphruk Road,";
        $kadokawaAdress2 = "Taling Chan, Bangkok 10170";
        $kadokawaAdress3 = "TEL. 02-434-0333";

        $this->logger->info('SetOrderToLobotAndResetStatus ---> TargetOrder id : ' . $orderId);
        $customerId = $order->getCustomerId();

        $shippingAddressObj = $order->getShippingAddress();
        $telephone = $clientName = $orderDeliveryPostCode = "None";
        $taxPrice = $grandTotal = 0;

        if ($shippingAddressObj) {
            $shippingAddressArray = $shippingAddressObj->getData();
            $telephone = $shippingAddressArray['telephone'];
            $clientName = $shippingAddressArray['firstname'] . ' ' . $shippingAddressArray['lastname'];

            $company = isset($shippingAddressArray['company']) ? $shippingAddressArray['company'] : "";
            $address1 = ' ' . $shippingAddressArray['street'];
            $address2 = ' ' . $shippingAddressArray['city'] . ' ' . $shippingAddressArray['region'];
            $address3 = ' ' . $shippingAddressArray['postcode'];

            $address = $company . $address1 . $address2 . $address3;

            $orderDeliveryPostCode = $shippingAddressArray['postcode'];
        }

        $products = [];
        $productsForCheck = [];
        $orderItems = $order->getAllItems();

        foreach ($orderItems as $product) {
            $productData = $product->getProduct();

            $loggerMessage = 'SetOrderToLobotAndResetStatus Try to get product id :: ' . $productData->getId() . ' ---> Name is :: ' . $productData->getName();
            $this->logger->info($loggerMessage);

            $stockStatus = $productData->getData('stock_status');
            $loggerMessage = 'SetOrderToLobotAndResetStatus Found product sku :: ' . $productData->getSku() . ' ---> Status is :: ' . $stockStatus;
            $this->logger->info($loggerMessage);

            $productsForCheck[$product->getSku()] = $product->getData('qty_ordered');

            $barcode = $productData->getData('barcode');

            $loggerMessage = 'SetOrderToLobotAndResetStatus Product is ready, Tax is :: ' . $product->getTaxAmount();
            $this->logger->info($loggerMessage);

            $taxPrice += $product->getTaxAmount();
            $itemPrice = $product->getData('price');
            $itemQty = $product->getData('qty_ordered');
            $grandTotal += $itemPrice * $itemQty;

            $productDataBean = [
                "record_indicator" => 'D',
                "item_number" => $product->getSku(),
                "product_number" => $barcode,
                "catagories" => $product->getCategoryIds(),
                "product_name" => $product->getName(),
                "price" => $itemPrice,
                "quantity" => $itemQty
            ];
            array_push($products, $productDataBean);
        }

        $grandTotal += $order->getShippingAmount();
        $reward_spend = $order->getRewardsSpentAmount() ?? 0;

        $orderDataBean = [
            'record_indicator' => 'H',
            "r3" => "3001",
            "unique_number" => $order->getIncrementId(),
            "order_number" => $order->getIncrementId(),
            "order_serial_number" => $order->getIncrementId(),
            "order_date" => date('Ymd', strtotime($order->getCreatedAt())),
            "order_member_number" => $customerId,
            "order_name" => $clientName,
            "order_order_address1" => $address1,
            "order_order_address2" => $address2,
            "order_order_address3" => $address3,
            "order_delivery_post_code" => $orderDeliveryPostCode,
            "order_phone_number" => $telephone,
            "destination_name" => $clientName,
            "delivery_address" => $address,
            "delivery_address1" => $address1,
            "delivery_address2" => $address2,
            "delivery_address3" => $address3,
            "destination_phone_number" => $telephone,
            "name_of_client" => $taxId,
            "name_of_client_local" => $kadokawaName,
            "client_address1" => $kadokawaAdress1,
            "client_address2" => $kadokawaAdress2,
            "client_address3" => $kadokawaAdress3,
            "client_address1_local" => $kadokawaAdress1,
            "client_address2_local" => $kadokawaAdress2,
            "client_address3_local" => $kadokawaAdress3,
            "requestor_phone_number" => $telephone,
            "shipping_amount" => $order->getShippingAmount(),
            "reward_spend" => $reward_spend,
            "discount_amount" => $order->getDiscountAmount(),
            "order_tax" => $taxPrice,
        ];

        $orderBean = [
            "header" => $orderDataBean,
            "detials" => $products
        ];

        $this->lobotOrderTsvBuilder->OrderToLobotTsvBuilder($orderBean);
        $loggerMessage = 'SetOrderToLobotAndResetStatus Order not contain PreOrder and send to Lobot :: ' . $orderId;
        $this->logger->info($loggerMessage);
    }
}
