<?php

namespace Aureatelabs\OrderExport\Model;

use Aureatelabs\OrderExport\Api\Data\OrderExportInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class OrderExport extends AbstractModel implements OrderExportInterface
{
    /**
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Define resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Aureatelabs\OrderExport\Model\ResourceModel\OrderExport::class);
    }

    /**
     * @return array|int|mixed|null
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @param mixed $id
     * @return OrderExportInterface
     */
    public function setId($id): OrderExportInterface
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * @param string $title
     * @return OrderExportInterface
     */
    public function setTitle(string $title): OrderExportInterface
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @param string $creationTime
     * @return OrderExportInterface
     */
    public function setCreationTime(string $creationTime): OrderExportInterface
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * @return string|null
     */
    public function getCreationTime(): ?string
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime(): ?string
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @param string $updateTime
     * @return OrderExportInterface
     */
    public function setUpdateTime(string $updateTime): OrderExportInterface
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }
}
