<?php

namespace Aureatelabs\OrderExport\Plugin;

class OrdersGrid
{
    public function afterGetReport($subject, $collection, $requestName)
    {
        if ($requestName !== 'sales_order_grid_data_source') {
            return $collection;
        }

        if ($collection->getMainTable() === $collection->getResource()->getTable('sales_order_grid'))
        {
            $orderAddressTable  = $collection->getResource()->getTable('sales_shipment');
            $collection->getSelect()->joinLeft(
                ['ss' => $orderAddressTable],
                'ss.order_id = main_table.entity_id',
                ['shipped_date' => 'MAX(ss.created_at)']
            );
            $collection->getSelect()->group('main_table.entity_id');
        }

        return $collection;
    }
}