<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\OrderExport\Controller\Adminhtml\Export\File;

use Aureatelabs\OrderExport\Model\OrderExport\CsvConsumer;
use Aureatelabs\OrderExport\Model\OrderExportRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Filesystem;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Filesystem\Directory\WriteFactory;

/**
 * Controller that delete file by name.
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Url to this controller
     */
    const URL = 'aureatelabs_order_export/export_file/delete';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var WriteFactory
     */
    private $writeFactory;

    /**
     * @var OrderExportRepository
     */
    private $orderExportRepository;

    /**
     * @param Context $context
     * @param Filesystem $filesystem
     * @param WriteFactory $writeFactory
     * @param OrderExportRepository $orderExportRepository
     */
    public function __construct(
        Context $context,
        Filesystem $filesystem,
        WriteFactory $writeFactory,
        OrderExportRepository $orderExportRepository
    ) {
        parent::__construct($context);
        $this->filesystem = $filesystem;
        $this->writeFactory = $writeFactory;
        $this->orderExportRepository = $orderExportRepository;
    }

    /**
     * Controller basic method implementation.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('aureatelabs_order_export/export/index');
        try {
            if (empty($fileName = $this->getRequest()->getParam('filename'))) {
                $this->messageManager->addErrorMessage(__('Please provide valid export file name'));

                return $resultRedirect;
            }
            $directoryWrite = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
            try {
                $id = $this->getRequest()->getParam('id');
                try {
                    $this->orderExportRepository->deleteById($id);
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__('There are no export file with such id %1', $id));
                    return $resultRedirect;
                }
                $directoryWrite->delete($directoryWrite->getAbsolutePath() . CsvConsumer::CSV_EXPORT_PATH . '/' . $fileName);
                $this->messageManager->addSuccessMessage(__('File %1 deleted', $fileName));
            } catch (ValidatorException | FileSystemException $exception) {
                $this->messageManager->addErrorMessage(
                    __('Sorry, but the data is invalid or the file is not uploaded.')
                );
            }
        } catch (FileSystemException $exception) {
            $fileName = $this->getRequest()->getParam('filename');
            $this->messageManager->addErrorMessage(__('There are no export file with such name %1', $fileName));
        }

        return $resultRedirect;
    }
}
