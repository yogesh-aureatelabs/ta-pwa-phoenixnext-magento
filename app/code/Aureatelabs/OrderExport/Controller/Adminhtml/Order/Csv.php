<?php

namespace Aureatelabs\OrderExport\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

class Csv extends Action implements HttpPostActionInterface
{
    /**
     * Queue Size
     */
    const QUEUE_SIZE = 5000;

    /**
     * Queue Topic Name
     */
    const QUEUE_TOPIC_NAME = 'al_order_export.export_to_csv';

    /**
     * Authorization level of a basic admin session
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::sales';

    /**
     * @var string
     */
    protected $redirectUrl = 'sales/order/index';

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var PublisherInterface
     */
    private $messagePublisher;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Queue constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param Json $json
     * @param PublisherInterface|null $publisher
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        Json $json,
        PublisherInterface $publisher = null
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->json = $json;
        $this->messagePublisher = $publisher ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(PublisherInterface::class);
    }

    /**
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            return $this->massAction($collection);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath($this->redirectUrl);
        }
    }

    /**
     * @param $collection
     * @return mixed
     */
    protected function massAction($collection)
    {
        $selectedOrders = $collection->getAllIds();
        $ordersArr = array_chunk($selectedOrders, self::QUEUE_SIZE);
        foreach ($ordersArr as $orders) {
            $newOrders = array_map('intval', $orders);
            $this->messagePublisher->publish(self::QUEUE_TOPIC_NAME, $this->json->serialize($newOrders));
        }
        $this->messageManager->addSuccessMessage(__('Orders are added in queue to generate CSV, You\'ll get CSV file soon.'));
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($this->redirectUrl);
    }
}
