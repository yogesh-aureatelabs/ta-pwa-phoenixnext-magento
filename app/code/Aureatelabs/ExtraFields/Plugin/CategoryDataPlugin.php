<?php

namespace Aureatelabs\ExtraFields\Plugin;

use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\Category as Resource;

class CategoryDataPlugin
{
    /**
     * @var \Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\Category
     */
    private $resourceModel;

    /**
     * BundleData constructor.
     *
     * @param Resource $resource
     * @param CollectionFactory $categoryCollectionFactory
     */
    public function __construct(Resource $resource, \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory)
    {
        $this->resourceModel = $resource;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * @param \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\CategoryData $subject
     * @param $result
     * @param $indexData
     * @param $storeId
     * @return mixed
     */
    public function afterAddData(
        \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\CategoryData $subject,
        $result,
        $indexData,
        $storeId
    ) {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addIsActiveFilter();
        $allCategories = [];
        foreach ($collection as $category) {
            $allCategories[] = $category->getId();
        }
        $categoryData = $this->resourceModel->loadCategoryData($storeId, array_keys($indexData));
        foreach ($categoryData as $categoryDataRow) {
            $productId = (int)$categoryDataRow['product_id'];
            $categoryId = (int)$categoryDataRow['category_id'];
            $position = (int)$categoryDataRow['position'];
            if (in_array($categoryDataRow['category_id'], $allCategories)) {
                $productCategoryData = [
                    'category_id' => $categoryId,
                    'name' => (string)$categoryDataRow['name'],
                    'position' => $position,
                    'is_active' => 1
                ];
            } else {
                $productCategoryData = [
                    'category_id' => $categoryId,
                    'name' => (string)$categoryDataRow['name'],
                    'position' => $position,
                    'is_active' => 0
                ];
            }
            if (!isset($indexData[$productId]['category'])) {
                $indexData[$productId]['category'] = [];
            }
            if (!isset($indexData[$productId]['category_ids'])) {
                $indexData[$productId]['category_ids'] = [];
            }
            $indexData[$productId]['category'][] = $productCategoryData;
            $indexData[$productId]['category_ids'][] = $categoryId;
        }
        $categoryData = null;
        return $indexData;
    }
}
