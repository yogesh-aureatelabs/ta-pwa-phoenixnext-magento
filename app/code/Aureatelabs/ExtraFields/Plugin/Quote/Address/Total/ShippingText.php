<?php

namespace Aureatelabs\ExtraFields\Plugin\Quote\Address\Total;

class ShippingText
{
    /**
     * Add shipping totals information to address object
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundfetch(
        \Magento\Quote\Model\Quote\Address\Total\Shipping $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        if (!$quote->getIsVirtual()) {
            $amount = $total->getShippingAmount();
            $shippingDescription = $total->getShippingDescription();
            if ($quote->getGrandTotal() > 800) {
                $title = ($shippingDescription)
                    ? __('ค่าส่ง (%1)', $shippingDescription)
                    : __('Shipping & Handling');
            } else {
                $title = ($shippingDescription)
                    ? __('Shipping & Handling (%1)', $shippingDescription)
                    : __('Shipping & Handling');
            }
            return [
                'code' => "shipping",
                'title' => $title,
                'value' => $amount
            ];
        } else {
            return [];
        }
    }
}
