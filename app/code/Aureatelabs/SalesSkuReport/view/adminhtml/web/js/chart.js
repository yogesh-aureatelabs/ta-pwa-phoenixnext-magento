/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
define(
    [ 'jquery', 'uiComponent', 'ko','libChart'],
    function($, Component, ko) {

        'use strict';


        ko.observableGroup = function(observables) {
            var observableManager = {};
            var throttle = 0;
            var throttleTimeout;

            observableManager.throttle = function(duration) {
                throttle = duration;
                return observableManager;
            };

            observableManager.subscribe = function(handler) {
                function throttledHandler(val) {
                    if(throttle > 0) {
                        if(!throttleTimeout) {
                            throttleTimeout = setTimeout(function() {
                                throttleTimeout = undefined;
                                handler(val);
                            }, throttle);
                        }
                    }
                    else
                    { handler(val); }
                }

                for(var i = 0; i < observables.length; i++)
                { observables[i].subscribe(throttledHandler); }

                return observableManager;
            };

            return observableManager;
        };

        var getType = function(obj) {
            if ((obj) && (typeof (obj) === "object") && (obj.constructor == (new Date).constructor)) return "date";
            return typeof obj;
        };

        var getSubscribables = function(model) {
            var subscribables = [];
            scanForObservablesIn(model, subscribables);
            return subscribables;
        };

        var scanForObservablesIn = function(model, subscribables){
            for (var parameter in model)
            {
                var typeOfData = getType(model[parameter]);
                switch(typeOfData)
                {
                    case "object": { scanForObservablesIn(model[parameter], subscribables); } break;
                    case "array":
                    {
                        var underlyingArray = model[parameter]();
                        underlyingArray.forEach(function(entry, index){ scanForObservablesIn(underlyingArray[index], subscribables); });
                    }
                    break;

                    default:
                    {
                        if(ko.isComputed(model[parameter]) || ko.isObservable(model[parameter]))
                        { subscribables.push(model[parameter]); }
                    }
                    break;
                }
            }
        };

        ko.bindingHandlers.chart = {
            init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                var allBindings = allBindingsAccessor();
                var chartBinding = allBindings.chart;
                var activeChart;
                var chartData;

                var createChart = function() {
                    var chartType = ko.unwrap(chartBinding.type);
                    var data = ko.toJS(chartBinding.data);
                    var options = ko.toJS(chartBinding.options);

                    chartData = {
                        type: chartType,
                        data: data,
                        //options: options
		        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    };

                    activeChart = new Chart(element, chartData);
                };

                var refreshChart = function() {
                    chartData.data = ko.toJS(chartBinding.data);
                    activeChart.update();
                    activeChart.resize();
                };

                var subscribeToChanges = function() {
                    var throttleAmount = ko.unwrap(chartBinding.options.throttle) || 100;
                    var dataSubscribables = getSubscribables(chartBinding.data);
                    console.log("found obs", dataSubscribables);

                    ko.observableGroup(dataSubscribables)
                        .throttle(throttleAmount)
                        .subscribe(refreshChart);
                };

                createChart();

                if(chartBinding.options && chartBinding.options.observeChanges)
                { subscribeToChanges(); }
            }
        };


        return Component
                .extend({
                    defaults : {
                        template : 'Aureatelabs_SalesSkuReport/chart'
                    },

                    initialize : function() {

                        var labelsdata = [];
                        jQuery("tbody .col-age").each(function () {
                            labelsdata.push(jQuery.trim(jQuery(this).html()));
                            jQuery(this).parents("tr").attr("age", jQuery.trim(jQuery(this).html()));
                        });

                        jQuery("tbody .col-gender").each(function () {
                            jQuery(this).parents("tr").attr("gender", jQuery.trim(jQuery(this).html()));
                        });

                        var result = [];
                        $.each(labelsdata, function(i, e) {
                            if ($.inArray(e, result) == -1) result.push(e);
                        });
                        
                        if (result.length == 0) {
                            result = ['Under 13', '13-17', '18-24', '25-34', '35-44', '45-54', '55-64', '65 Over'];
                        }
			    
			var qtys = [];
                        jQuery("tbody .col-qty").each(function () {
                            qtys.push(jQuery.trim(jQuery(this).html()));
                            jQuery(this).parents("tr").attr("qty", jQuery.trim(jQuery(this).html()));
                        });

                        var male = [];
                        var female = [];
                        $.each(result, function(i, e) {
                            if(jQuery('tbody tr[age="' + e +'"][gender="Male"]').attr('qty')) {
                                male.push(jQuery('tbody tr[age="' + e +'"][gender="Male"]').attr('qty'))
                            } else {
                                male.push(0);
                            }
                            //male.push(jQuery('tbody tr[age="' + e +'"][gender="Male"]').attr('qty'));
                            if(jQuery('tbody tr[age="' + e +'"][gender="Female"]').attr('qty')) {
                                female.push(jQuery('tbody tr[age="' + e +'"][gender="Female"]').attr('qty'));
                            } else {
                                female.push('0');
                            }
                        });

                        this._super();
                        this.SimpleLineData = {                            
                                labels: result,
                                datasets: [
                                    {
                                        /* label: "Sold Quantity",
                                        pointStrokeColor: "#fff",
                                        pointHighlightFill: "#fff",
                                        pointHighlightStroke: "rgba(220,220,220,1)",
                                        data: qtys */
                                        label: "Male",
                                        backgroundColor: "blue",
                                        data: male
                                    }, {
                                        label: "Female",
                                        backgroundColor: "red",
                                        data: female
                                    }
                                ]
                            };

                        /* Doughnut and Pie */
                        this.RedValue = ko.observable(300);
                        this.GreenValue = ko.observable(50);
                        this.YellowValue = ko.observable(100);
                        this.DynamicDoughnutData = {
                            labels: ["Red", "Green", "Yellow" ],
                            datasets: [
                                {
                                    data: [this.RedValue, this.GreenValue, this.YellowValue],
                                    backgroundColor: [
                                        "#FF6384",
                                        "#36A2EB",
                                        "#FFCE56"
                                    ],
                                    hoverBackgroundColor: [
                                        "#FF6384",
                                        "#36A2EB",
                                        "#FFCE56"
                                    ]
                                }]
                        };
                    }
                });
    });
