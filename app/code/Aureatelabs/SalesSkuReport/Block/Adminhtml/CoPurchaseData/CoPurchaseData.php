<?php
namespace Aureatelabs\SalesSkuReport\Block\Adminhtml\CoPurchaseData;

use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\CollectionFactory;
use Magento\Catalog\Model\ProductFactory;

class CoPurchaseData extends Extended {
    protected $registry;

    protected $collectionFactory;

    protected $_objectManager = null;

    protected $resourceConnection;

    protected $productFactory;

    public function __construct(
        Context $context,
        Data $backendHelper,
        Registry $registry,
        ObjectManagerInterface $objectManager,
        CollectionFactory $collectionFactory,
        ResourceConnection $resourceConnection,
        ProductFactory $productFactory,
        array $data = []
    ) {
        $this->_objectManager = $objectManager;
        $this->collectionFactory = $collectionFactory;
        $this->registry = $registry;
        $this->resourceConnection = $resourceConnection;
        $this->productFactory = $productFactory;
        parent::__construct($context, $backendHelper, $data);
        $this->setFilterVisibility(false);
    }

    protected function _construct() {
        parent::_construct();
    }

    protected function _prepareColumns() {

        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'type' => 'text',
                'index' => 'sku',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'sortable'  => false
            ]
        );

        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'type' => 'text',
                'index' => 'name',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'sortable'  => false
            ]
        );

        $this->addColumn(
            'releasedate',
            [
                'header' => __('Release Date'),
                'type' => 'date',
                'index' => 'releasedate',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'sortable'  => false
            ]
        );

        $this->addColumn(
            'qty',
            [
                'header' => __('Qty'),
                'type' => 'number',
                'index' => 'qty',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'filter' => false,
                'sortable'  => false
            ]
        );

        $this->addExportType('*/*/exportCoPurchasedItems', __('CSV'));

        return parent::_prepareColumns();
    }

    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->create();
        if($_GET)
        {
            $sku = $_GET['sku'];
            $fromDate = $_GET['from_date'];
            $toDate = $_GET['to_date'];

            $startDate = date("Y-m-d h:i:s",strtotime($fromDate));
            $endDate = date("Y-m-d h:i:s", strtotime($toDate));

            $query = "SELECT `order_id` FROM `sales_order_item` WHERE `sku` = '". $sku . "' AND (`created_at` >= '". $startDate ."') AND (`created_at` <= '". $endDate ."' )";;
            $results = $this->resourceConnection->getConnection()->fetchAll($query);

            if ($results) {
                $orderIds = implode(', ', array_column($results, 'order_id'));
                $q = "SELECT `sku`,COUNT(*) as qty FROM `sales_order_item` WHERE `sku`!= '" . $sku .  "' AND `order_id` IN (".  $orderIds.") GROUP BY sku HAVING COUNT(*) > 1 ORDER BY COUNT(*) DESC LIMIT 20";
                $r = $this->resourceConnection->getConnection()->fetchAll($q);

                foreach ($r as $item) {
                    $product = $this->productFactory->create();
                    $product->load($product->getIdBySku($item['sku']));
                    $item['name'] = $product->getName();
                    $item['releasedate'] = $product->getCreatedAt();
                    $varienObject = new \Magento\Framework\DataObject();
                    $varienObject->setData($item);
                    $collection->addItem($varienObject);
                }
            }
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareLayout()
    {
        $this->unsetChild('reset_filter_button');
        $this->unsetChild('search_button');

        return parent::_prepareLayout();
    }
}