<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Model\Config\Source;

class Year implements \Magento\Framework\Data\OptionSourceInterface {

    public function toOptionArray()
    {
        
        $data = array();
        $return = array();

        $return[0] = ['value' => '0', 'label' => __('Select Year')];
        for($i = 1; $i <= 10; $i++) {
            $return[$i] = ['value' => $i, 'label' => $i];
        }

        return $return;
    }

}
