<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Nosto
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2021 Aureate Labs. ( https://aureatelabs.com )
*/

namespace Aureatelabs\Nosto\Observer\Product;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Load implements ObserverInterface {

    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(Observer $observer) {
        $product = $observer->getProduct();
        $magentoProduct = $this->productRepository->getById($product->getProductId());
        $sku = $magentoProduct->getSku();
        $product->setProductId($sku);
    }
}
