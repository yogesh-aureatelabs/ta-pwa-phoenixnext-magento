<?php

namespace Aureatelabs\CustomerStatus\Plugin;

use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;

class CheckCustomerStatus
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * CheckCustomerStatus constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     */
    public function __construct(\Magento\Customer\Api\CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param \Magento\Integration\Model\CustomerTokenService $subject
     * @param $username
     * @param $password
     * @return array
     * @throws InvalidEmailOrPasswordException
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function beforeCreateCustomerAccessToken(
        \Magento\Integration\Model\CustomerTokenService $subject,
        $username,
        $password
    ) {
        try {
            $customer = $this->customerRepository->get($username);
        } catch (NoSuchEntityException $e) {
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }
        if ($customer->getCustomAttribute('customer_enabled') !== NULL && $customer->getCustomAttribute('customer_enabled')->getValue() == 0) {
            throw new LocalizedException(__('The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later.'));
        }
        return [$username, $password];
    }
}
