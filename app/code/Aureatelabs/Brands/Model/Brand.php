<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Model;

use Aureatelabs\Brands\Api\Data\BrandInterface;

/**
 * Class Banner
 * @package Aureatelabs\Brands\Model
 */
class Brand extends \Magento\Framework\Model\AbstractModel implements BrandInterface
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'aureate_brand_brand';

    /**
     * @var string
     */
    protected $_cacheTag = 'aureate_brand_brand';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'aureate_brand_brand';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aureatelabs\Brands\Model\ResourceModel\Brand');
    }

    /**
     * Get BrandId.
     *
     * @return int|mixed
     */
    public function getBrandId()
    {
        return $this->getData(self::BRAND_ID);
    }

    /**
     * Set BrandId.
     *
     * @param int $brandId
     * @return Brand|\Magento\Framework\Model\AbstractModel
     */
    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }

    /**
     * Get Brand Name.
     */
    public function getBrandName()
    {
        return $this->getData(self::BRAND_NAME);
    }

    /**
     * Set Brand Name.
     *
     * @param $brandName
     * @return Brand
     */
    public function setBrandName($brandName)
    {
        return $this->setData(self::BRAND_NAME, $brandName);
    }

    /**
     * Get Image.
     */
    public function getBrandImage()
    {
        return $this->getData(self::BRAND_IMAGE);
    }

    /**
     * Set Image.
     *
     * @param $brandImage
     * @return Brand
     */
    public function setBrandImage($brandImage)
    {
        return $this->setData(self::BRAND_IMAGE, $brandImage);
    }

    /**
     * Get Brand Products.
     */
    public function getBrandProducts()
    {
        return $this->getData(self::BRAND_PRODUCTS);
    }

    /**
     * Set Brand Products.
     *
     * @param $brandProducts
     * @return Brand
     */
    public function setBrandProducts($brandProducts)
    {
        return $this->setData(self::BRAND_PRODUCTS, $brandProducts);
    }

    /**
     * Get Active Status.
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set Active Status.
     *
     * @param $isActive
     * @return Brand
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get Creation Time.
     *
     * @return \Aureatelabs\Banner\Api\Data\varchar|mixed
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set Creation Time.
     *
     * @param $createdAt
     * @return Brand
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get Modification Time.
     *
     * @return \Aureatelabs\Banner\Api\Data\varchar|mixed
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATE_AT);
    }

    /**
     * Set Modification Time.
     *
     * @param $updatedAt
     * @return Brand
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATE_AT, $updatedAt);
    }

     /**
     * Get Brand Description.
     */
    public function getBrandDescription()
    {
        return $this->getData(self::BRAND_DESCRIPTION);
    }

    /**
     * Set Brand Description.
     *
     * @param $brandDescription
     * @return Brand
     */
    public function setBrandDescription($brandDescription)
    {
        return $this->setData(self::BRAND_DESCRIPTION, $brandDescription);
    }

    /**
     * Get Feature Status.
     */
    public function getIsFeature()
    {
        return $this->getData(self::IS_FEATURE);
    }

    /**
     * Set Feature Status.
     *
     * @param $isFeature
     * @return Brand
     */
    public function setIsFeature($isFeature)
    {
        return $this->setData(self::IS_FEATURE, $isFeature);
    }

    /**
     * Get Link.
     */
    public function getLink()
    {
        return $this->getData(self::LINK);
    }

    /**
     * Set Link.
     *
     * @param $link
     * @return Brand
     */
    public function setLink($link)
    {
        return $this->setData(self::LINK, $link);
    }

    /**
     * Get Image.
     */
    public function getBrandDetailImage()
    {
        return $this->getData(self::BRAND_DETAIL_IMAGE);
    }

    /**
     * Set Image.
     *
     * @param $brandImage
     * @return Brand
     */
    public function setBrandDetailImage($brandDetailImage)
    {
        return $this->setData(self::BRAND_DETAIL_IMAGE, $brandDetailImage);
    }

    /**
     * Get Image.
     */
    public function getBrandMobileImage()
    {
        return $this->getData(self::BRAND_MOBILE_IMAGE);
    }

    /**
     * Set Image.
     *
     * @param $brandMobileImage
     * @return Brand
     */
    public function setBrandMobileImage($brandMobileImage)
    {
        return $this->setData(self::BRAND_MOBILE_IMAGE, $brandMobileImage);
    }

    /**
     * Get Meta Title.
     */
    public function getMetaTitle()
    {
        return $this->getData(self::META_TITLE);
    }

    /**
     * Set Meta Title.
     *
     * @param $metaTitle
     * @return Brand
     */
    public function setMetaTitle($metaTitle)
    {
        return $this->setData(self::META_TITLE, $metaTitle);
    }

    /**
     * Get Meta Keyword.
     */
    public function getMetaKeyword()
    {
        return $this->getData(self::META_KEYWORD);
    }

    /**
     * Set Meta Keyword.
     *
     * @param $metaKeyword
     * @return Brand
     */
    public function setMetaKeyword($metaKeyword)
    {
        return $this->setData(self::META_KEYWORD, $metaKeyword);
    }

    /**
     * Get Meta Description.
     */
    public function getMetaDescription()
    {
        return $this->getData(self::META_DESCRIPTION);
    }

    /**
     * Set Meta Description.
     *
     * @param $metaDescription
     * @return Brand
     */
    public function setMetaDescription($metaDescription)
    {
        return $this->setData(self::META_DESCRIPTION, $metaDescription);
    }

    /**
     * Get Og Title.
     */
    public function getOgTitle()
    {
        return $this->getData(self::OG_TITLE);
    }

    /**
     * Set Og Title.
     *
     * @param $ogTitle
     * @return Brand
     */
    public function setOgTitle($ogTitle)
    {
        return $this->setData(self::OG_TITLE, $ogTitle);
    }

    /**
     * Get Og Image.
     */
    public function getOgImage()
    {
        return $this->getData(self::OG_IMAGE);
    }

    /**
     * Set Og Image.
     *
     * @param $ogImage
     * @return Brand
     */
    public function setOgImage($ogImage)
    {
        return $this->setData(self::OG_IMAGE, $ogImage);
    }

    /**
     * Get Og Description.
     */
    public function getOgDescription()
    {
        return $this->getData(self::OG_DESCRIPTION);
    }

    /**
     * Set Og Description.
     *
     * @param $ogDescription
     * @return Brand
     */
    public function setOgDescription($ogDescription)
    {
        return $this->setData(self::OG_DESCRIPTION, $ogDescription);
    }

    /**
     * Get Og Type.
     */
    public function getOgType()
    {
        return $this->getData(self::OG_TYPE);
    }

    /**
     * Set Og Type.
     *
     * @param $ogType
     * @return Brand
     */
    public function setOgType($ogType)
    {
        return $this->setData(self::OG_TYPE, $ogType);
    }
}
