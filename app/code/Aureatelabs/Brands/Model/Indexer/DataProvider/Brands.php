<?php

namespace Aureatelabs\Brands\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Cms\Model\Template\FilterProvider;

class Brands implements DataProviderInterface
{
    /**
     * @var FilterProvider
     */
    private $filterProvider;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param FilterProvider $filterProvider
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        FilterProvider $filterProvider,
        CollectionFactory $collectionFactory
    ) {
        $this->filterProvider = $filterProvider;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId): array
    {
        $enabledProducts = $this->getEnableProducts();
        $templateFilter = $this->filterProvider->getPageFilter();
        foreach ($indexData as &$brand) {
            if (!empty($brand['brand_products'])) {
                $brandProducts = explode(',', $brand['brand_products']);
                $brandAvailableProduct = [];
                foreach ($brandProducts as $brandProduct) {
                    if (in_array($brandProduct, $enabledProducts)) {
                        $brandAvailableProduct[] = $brandProduct;
                    }
                }
                $brand['brand_products'] = !empty($brandAvailableProduct) ? implode(',', $brandAvailableProduct) : null;
            }
            $brand['brand_description'] = $templateFilter->filter($brand['brand_description']);
            $brand['is_active'] = 1;
        }
        return $indexData;
    }

    /**
     * @return array
     */
    private function getEnableProducts(): array
    {
        $products = $this->collectionFactory->create()->addAttributeToFilter('status', ['eq' => 1]);
        foreach ($products as $product) {
            $enabledProducts[] = $product->getId();
        }
        return $enabledProducts ?? [];
    }
}
