<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\Brands\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package Aureatelabs\Brands\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.1.0', '<=')) {
            $this->addLinkFeature($setup);
        }
        if (version_compare($context->getVersion(), '1.2.0', '<=')) {
            $this->addBrandDetailImageFeature($setup);
        }
        if (version_compare($context->getVersion(), '1.3.0', '<=')) {
            $this->addBrandMobileImageFeature($setup);
        }
        if (version_compare($context->getVersion(), '1.4.0', '<=')) {
            $this->addBrandSortOrder($setup);
        }
        if (version_compare($context->getVersion(), '1.5.0', '<=')) {
            $this->addMetaFields($setup);
        }
        if (version_compare($context->getVersion(), '1.5.0', '<=')) {
            $this->addOGMetaFields($setup);
        }
    }

    /**
     * @param $setup
     */
    public function addLinkFeature($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'is_feature',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'default' => 0,
                    'comment' => 'Is Feature'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'link',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' => 'Link'
                ]
            );
        }

        $installer->endSetup();
    }

    /**
     * @param $setup
     */
    public function addBrandDetailImageFeature($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'brand_detail_image',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'Brand Detail Image'
                ]
            );
        }
        $installer->endSetup();
    }

    public function addBrandMobileImageFeature($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'brand_mobile_image',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'Brand Mobile Image'
                ]
            );
        }
        $installer->endSetup();
    }

    /**
     * @param $setup
     */
    public function addBrandSortOrder($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'sort_order',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'default' => 0,
                    'comment' => 'Sort Order'
                ]
            );
        }
        $installer->endSetup();
    }

    /**
     * @param $setup
     */
    public function addMetaFields($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'meta_title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'Meta Title'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'meta_keywords',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'Meta Title'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'meta_description',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'Meta Description'
                ]
            );
        }
        $installer->endSetup();
    }

    /**
     * @param $setup
     */
    public function addOGMetaFields($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'og_title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'OG Title'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'og_description',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'OG Description'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'og_image',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'OG Image'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'og_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 555,
                    'comment' => 'OG Type'
                ]
            );
        }
        $installer->endSetup();
    }
}
