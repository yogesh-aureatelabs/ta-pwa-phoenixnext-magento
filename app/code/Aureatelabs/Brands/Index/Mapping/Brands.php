<?php

/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Brands
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\Brands\Index\Mapping;

use Divante\VsbridgeIndexerCore\Api\MappingInterface;
use Divante\VsbridgeIndexerCore\Api\Mapping\FieldInterface;
use Magento\Framework\Event\ManagerInterface as EventManager;

/**
 * Class Brands
 * @package Aureatelabs\Brands\Index\Mapping
 */
class Brands implements MappingInterface
{
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $textFields = [
        'brand_name',
        'brand_image',
        'brand_description',
        'brand_products',
        'brand_detail_image',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type'
    ];

    /**
     * CmsBlock constructor.
     *
     * @param EventManager $eventManager
     */
    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    /**
     * @inheritdoc
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @inheritdoc
     */
    public function getMappingProperties()
    {
        $properties = [
            'brand_id' => ['type' => FieldInterface::TYPE_LONG],
            'is_active' => ['type' => FieldInterface::TYPE_TEXT],
            'id' => ['type' => FieldInterface::TYPE_TEXT, 'fielddata' => true],
            'sort_order' => ['type' => FieldInterface::TYPE_TEXT, 'fielddata' => true]
        ];

        foreach ($this->textFields as $field) {
            $properties[$field] = ['type' => FieldInterface::TYPE_TEXT];
        }

        $mappingObject = new \Magento\Framework\DataObject();
        $mappingObject->setData('properties', $properties);

        $this->eventManager->dispatch(
            'elasticsearch_brands_mapping_properties',
            ['mapping' => $mappingObject]
        );

        return $mappingObject->getData();
    }
}
