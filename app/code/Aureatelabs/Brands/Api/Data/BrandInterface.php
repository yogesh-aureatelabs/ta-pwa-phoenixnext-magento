<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Api\Data;

interface BrandInterface {

    const BRAND_ID = 'brand_id';
    const BRAND_NAME = 'brand_name';
    const BRAND_DESCRIPTION = 'brand_description';
    const BRAND_IMAGE = 'brand_image';
    const BRAND_DETAIL_IMAGE = 'brand_detail_image';
    const BRAND_MOBILE_IMAGE = 'brand_mobile_image';
    const BRAND_PRODUCTS = 'brand_products';
    const IS_ACTIVE = 'is_active';
    const UPDATE_AT = 'updated_at';
    const CREATED_AT = 'created_at';
    const IS_FEATURE = 'is_feature';
    const LINK = 'link';
    const META_TITLE = 'meta_title';
    const META_KEYWORD = 'meta_keywords';
    const META_DESCRIPTION = 'meta_description';
    const OG_TITLE = 'og_title';
    const OG_DESCRIPTION = 'og_description';
    const OG_IMAGE = 'og_image';
    const OG_TYPE = 'og_type';

    /* Get BrandId. */
    public function getBrandId();

    /* Set BrandId */
    public function setBrandId($brandId);

    /* Get Brand Name. */
    public function getBrandName();

    /* Set Brand Name. */
    public function setBrandName($brandName);

    /* Get Image. */
    public function getBrandImage();

    /* Set Image. */
    public function setBrandImage($brandImage);

    /* Get Brand Products. */
    public function getBrandProducts();

    /* Set Brand Products. */
    public function setBrandProducts($brandProducts);

    /* Get Active Status */
    public function getIsActive();

    /* Set Active Status */
    public function setIsActive($isActive);

    /* Get Feature Status */
    public function getIsFeature();

    /* Set Feature Status */
    public function setIsFeature($isFeature);

    /* Get Creation Time. */
    public function getCreatedAt();

    /* Set Creation Time */
    public function setCreatedAt($createdAt);

    /* Get Modification Time. */
    public function getUpdatedAt();

    /* Set Modification Time */
    public function setUpdatedAt($updatedAt);

    /* Get Brand Description. */
    public function getBrandDescription();

    /* Set Brand Description. */
    public function setBrandDescription($brandDescription);

    /* Get link. */
    public function getLink();

    /* Set link. */
    public function setLink($link);

    /* Get Image. */
    public function getBrandDetailImage();

    /* Set Image. */
    public function setBrandDetailImage($brandDetailImage);

    /* Get Image. */
    public function getBrandMobileImage();

    /* Set Image. */
    public function setBrandMobileImage($brandMobileImage);

    /* Get meta. */
    public function getMetaTitle();

    /* Set meta title. */
    public function setMetaTitle($metaTitle);

    /* Get meta. */
    public function getMetaKeyword();

    /* Set meta keyword. */
    public function setMetaKeyword($metaKeyword);

    /* Get meta. */
    public function getMetaDescription();

    /* Set meta description. */
    public function setMetaDescription($metaDescription);

    /* Get og title. */
    public function getOgTitle();

    /* Set og title. */
    public function setOgTitle($ogTitle);

    /* Get og image. */
    public function getOgImage();

    /* Set og image. */
    public function setOgImage($ogImage);

    /* Get og description. */
    public function getOgDescription();

    /* Set og description. */
    public function setOgDescription($ogDescription);

    /* Get og type. */
    public function getOgType();

    /* Set og type. */
    public function setOgType($ogType);
}
