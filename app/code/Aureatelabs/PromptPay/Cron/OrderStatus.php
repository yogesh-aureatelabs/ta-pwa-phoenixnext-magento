<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\PromptPay\Cron;

use Magento\Sales\Model\Order;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SortOrder;

/**
 * Class OrderStatus
 * 
 * Cron job to check order status periodically
 */
class OrderStatus extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * @var \Aureatelabs\PromptPay\Model\Transaction\Inquiry
     */
    private $inquiry;

    /**
     * @var \Aureatelabs\PromptPay\Helper\Config
     */
    private $config;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Sales\Api\OrderManagementInterface
     */
    protected $orderManagement;

    /**
     * @var SortOrderBuilder
     */
    protected $sortOrder;

    /**
     * Constructor OrderStatus
     * 
     * @param \Aureatelabs\PromptPay\Model\Transaction\Inquiry $inquiry
     * @param \Aureatelabs\PromptPay\Helper\Config $config
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrder
     * @param \Magento\Sales\Api\OrderManagementInterface $orderManagement
     */
    public function __construct(
        \Aureatelabs\PromptPay\Model\Transaction\Inquiry $inquiry,
        \Aureatelabs\PromptPay\Helper\Config $config,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrder,
        \Magento\Sales\Api\OrderManagementInterface $orderManagement
    ) {
        $this->inquiry = $inquiry;
        $this->config = $config;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrder = $sortOrder;
        $this->orderManagement = $orderManagement;
    }

    /**
     * Entry point for cronjob
     */
    public function run()
    {
        if ($this->config->isEnable() && $this->config->isCronEnable()) {

            /** @var SortOrder $sortOrder */
            $sortOrder = $this->sortOrder->setField('created_at')
                ->setDirection(SortOrder::SORT_DESC)
                ->create();

            $date = (new \DateTime())->modify('-2 day');
            $sDate = (new \DateTime())->modify('-30 day');

            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter('status', ['pending','Pending_2C2P'], 'in')
                ->addFilter('created_at', $sDate->format('Y-m-d'), 'gt')
                ->addFilter('created_at', $date->format('Y-m-d'), 'lt')
                ->setSortOrders([$sortOrder])
                ->setPageSize(10)
                ->create();

            $orders = $this->orderRepository->getList($searchCriteria);
            foreach ($orders->getItems() as $order) {
                $result = $this->inquiry->send($order->getIncrementId());
                $this->update($result, $order);
            }
        }
    }

    /**
     * Fetch Single order and update it's status
     */
    public function processSingleOrder($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        $result = $this->inquiry->send($order->getIncrementId());
        $this->update($result, $order);
    }

    /**
     * Check 2C2P response code and update order status accordingly.
     * 
     * @param array $result
     * @param \Magento\Sales\Model\Order $order
     */
    public function update($result, $order)
    {
        if(!$this->inquiry->verifyHash($result)) {
            $this->setOrderStatus($order, Order::STATUS_FRAUD);
        }

        $code = $result['status'];
        if(strcasecmp($code, "A") == 0 || $code == 'A') {
            //IF payment status code is success
            $this->setOrderStatus($order, Order::STATE_PROCESSING);
        } else if(in_array($code, ['PF', 'AR', 'FF', 'IP', 'ROE', 'V', 'EX'])) {
            //If payment status code is cancel/Error/other.
            $this->orderManagement->cancel($order->getId());
            $this->setOrderStatus($order, Order::STATE_CANCELED);
        }
    }

    /**
     * Set order status.
     * 
     * @param \Magento\Sales\Model\Order $order
     * @param string $status
     */
    private function setOrderStatus($order, $status)
    {
        if ($order->getId()) {
            $order->setState($status);
            $order->setStatus($status);
            $order->save();
        }
    }
}
