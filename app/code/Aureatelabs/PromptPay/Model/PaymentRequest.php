<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\PromptPay\Model;

/**
 * Class PaymentRequest
 * 
 * Prepared payment request
 */
class PaymentRequest
{
    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $merchant;

    /**
     * Process Type:
	 *   I = transaction inquiry
	 *   V = transaction void
	 *   R = transaction Refund
	 *   S = transaction Settlement 
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $invoice;

    /**
     * @var string
     */
    private $secret;

    /**
     * Bind data with class properties based on passed values
     * 
     * @param array $data
     */
    public function setData($data)
    {
        if (count($data))
            foreach($data as $field => $value)
                $this->{$field} = $value;
    }

    /**
     * Encode request with Base64 algorithm
     * 
     * @return string
     */
    public function prepare()
    {
        return base64_encode(
            $this->getXML()
        );
    }

    /**
     * Generate sha1 Hash string from secret values
     * 
     * @return string
     */
    public function getHash()
    {
        $string = $this->concat();
        return strtoupper(hash_hmac(
            'sha1',
            $string,
            $this->secret,
            false
        ));
    }

    /**
     * Construct signature string
     * 
     * @return string
     */
    public function concat()
    {
        return implode('', array(
            $this->version,
            $this->merchant,
            $this->type,
            $this->invoice
        ));
    }

    /**
     * Generate XML request, fill mandatory data
     * Construct request message
     * 
     * @return string
     */
    public function getXML()
    {
        $hash = $this->getHash();
        $xml = <<<XML
<PaymentProcessRequest>
    <version>$this->version</version> 
    <merchantID>$this->merchant</merchantID>
    <processType>$this->type</processType>
    <invoiceNo>$this->invoice</invoiceNo> 
    <hashValue>$hash</hashValue>
</PaymentProcessRequest>
XML;
        return $xml;
    }

    /**
     * Contact response data and convert to Hash string
     * 
     * @return string
     */
    public function getResultHash($result)
    {
        $result = array_filter($result);
        $string = implode('', [
            $result['version'] ?? '',               $result['respCode'] ?? '',
            $result['processType'] ?? '',           $result['invoiceNo'] ?? '',
            $result['amount'] ?? '',                $result['status'] ?? '',
            $result['approvalCode'] ?? '',          $result['referenceNo'] ?? '',
            $result['transactionDateTime'] ?? '',   $result['paidAgent'] ?? '',
            $result['paidChannel'] ?? '',           $result['maskedPan'] ?? '',
            $result['eci'] ?? '',                   $result['paymentScheme'] ?? '',
            $result['processBy'] ?? '',             $result['refundReferenceNo'] ?? '',
            $result['userDefined1'] ?? '',          $result['userDefined2'] ?? '',
            $result['userDefined3'] ?? '',          $result['userDefined4'] ?? '',
            $result['userDefined5'] ?? ''
        ]);

        return strtoupper(hash_hmac(
            'sha1',
            $string,
            $this->secret,
            false
        ));
    }
}
