<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\PromptPay\Model\Transaction;

/**
 * Class Inquiry
 */
class Inquiry
{

    /**
     * @var \Aureatelabs\PromptPay\Model\PaymentRequest
     */
    private $paymentRequest;

    /**
     * @var \Aureatelabs\PromptPay\Helper\Http
     */
    private $http;

    /**
     * @var \Aureatelabs\PromptPay\Helper\Config
     */
    private $config;

    /**
     * Constructor Inquiry
     * 
     * @param \Aureatelabs\PromptPay\Model\PaymentRequest $paymentRequest
     * @param \Aureatelabs\PromptPay\Helper\Http $http
     * @param \Aureatelabs\PromptPay\Helper\Config $config
     */
    function __construct(
        \Aureatelabs\PromptPay\Model\PaymentRequest $paymentRequest,
        \Aureatelabs\PromptPay\Helper\Http $http,
        \Aureatelabs\PromptPay\Helper\Config $config
    ) {
        $this->paymentRequest = $paymentRequest;
        $this->http = $http;
        $this->config = $config;
    }

    /**
     * Send request to 2C2P PGW and get back response
     * 
     * @param string $invoice
     * @return json
     */
    public function send($invoice)
    {
        $this->paymentRequest->setData([
            'merchant' => $this->config->getId(),
            'secret' => $this->config->getSecret(),
            'version' => $this->config->getVersion(),
            'type' => $this->config->getType(),
            'invoice' => $invoice
        ]);

        $payload = $this->paymentRequest->prepare();
        $response = $this->http->post(
            $this->config->getUrl(),
            'paymentRequest=' . $payload
        );

        return $this->parse($response);
    }

    /**
     * Decode Base64 string and parse XML string
     * 
     * @param string $response
     * @return json
     */
    private function parse($response)
    {
        $xml = simplexml_load_string(
            base64_decode($response)
        );

        return $this->xmlToArray($xml);
    }

    /**
     * Convert XML object into json format
     * 
     * @param string SimpleXMLElement
     * @return json
     */
    private function xmlToArray($xml)
    {
        return json_decode(
            json_encode(
                (array)$xml
            ),true
        );
    }

    /**
     * Validate Hash string
     * @return bool
     */
    public function verifyHash($result)
    {
        $hash = $this->paymentRequest->getResultHash($result);
        return ($result['hashValue'] == strtolower($hash));
    }
}
