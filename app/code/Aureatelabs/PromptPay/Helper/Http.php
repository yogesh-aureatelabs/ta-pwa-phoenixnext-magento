<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\PromptPay\Helper;

/**
 * Class Http
 */
class Http
{

    /**
     * Post cURL request
     */
    public function post($url, $fields)
    {
        //open connection
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        //execute post
        $result = curl_exec($ch); //close connection
        curl_close($ch);

        return $result;
    } 
}
