<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aureatelabs\PromptPay\Helper;

/**
 * Class Config
 */
class Config
{

    const CONFIG_API_ON = 'payment/promptpay/active';

    const CONFIG_API_ID = 'payment/promptpay/merchant';

    const CONFIG_API_KEY = 'payment/promptpay/secret';

    const CONFIG_API_URL = 'payment/promptpay/url';

    const CONFIG_API_VER = 'payment/promptpay/version';

    const CONFIG_API_TYPE = 'payment/promptpay/type';

    const CONFIG_CRON_ON = 'payment/promptpay/cron';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $scopeConfig;

    /**
     * Config constructor
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Check module is Enable/Disable
     * @return bool
     */
    public function isEnable()
    {
        $status = $this->scopeConfig->getValue(
            self::CONFIG_API_ON
        );

        return !empty($status);
    }

    /**
     * Check module is Enable/Disable
     * @return bool
     */
    public function isCronEnable()
    {
        $status = $this->scopeConfig->getValue(
            self::CONFIG_CRON_ON
        );

        return !empty($status);
    }

    /**
     * Get MerchantID
     * @return string
     */
    public function getId()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_ID
        );
    }

    /**
     * Get SecretKey from 2C2P PGW Dashboard
     * @return string
     */
    public function getSecret()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_KEY
        );
    }

    /**
     * Get 2C2P PGW url
     * @return string
     */
    public function getUrl()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_URL
        );
    }

    /**
     * Get API version number
     * @return string
     */
    public function getVersion()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_VER
        );
    }

    /**
     * Get Process Type
     * @return string
     */
    public function getType()
    {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_TYPE
        );
    }
}
