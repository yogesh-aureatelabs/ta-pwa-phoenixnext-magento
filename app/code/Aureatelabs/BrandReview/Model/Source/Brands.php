<?php

namespace Aureatelabs\BrandReview\Model\Source;

use Aureatelabs\Brands\Model\ResourceModel\Brand\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class Brands implements OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     */
    public function getOptionArray(): array
    {
        $collection = $this->collectionFactory->create()->addFieldToSelect(['brand_id', 'brand_name']);
        $options = [];
        foreach ($collection as $item) {
            $options[$item->getBrandId()] = $item->getBrandName();
        }
        return $options;
    }

    /**
     * @return array
     */
    public function getAllOptions(): array
    {
        $result = $this->getOptions();
        array_unshift($result, ['value' => '', 'label' => '']);
        return $result;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        $result = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result ?? [];
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray(): array
    {
        return $this->getOptions();
    }
}
