<?php

namespace Aureatelabs\BrandReview\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Status implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function getOptionArray(): array
    {
        return [
            '0' => __('Pending'),
            '1' => __('Approved'),
            '2' => __('Not Approved'),
        ];
    }

    /**
     * @return array
     */
    public function getAllOptions(): array
    {
        $result = $this->getOptions();
        array_unshift($result, ['value' => '', 'label' => '']);
        return $result;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result ?? [];
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray(): array
    {
        return $this->getOptions();
    }
}
