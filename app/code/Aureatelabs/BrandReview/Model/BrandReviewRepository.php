<?php

namespace Aureatelabs\BrandReview\Model;

use Aureatelabs\BrandReview\Api\BrandReviewRepositoryInterface;
use Aureatelabs\BrandReview\Api\Data;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Aureatelabs\BrandReview\Model\ResourceModel\BrandReview\CollectionFactory as BrandReviewCollectionFactory;
use Aureatelabs\BrandReview\Model\ResourceModel\BrandReview as BrandReviewResource;
use Aureatelabs\BrandReview\Api\Data\BrandReviewSearchResultsInterfaceFactory;

class BrandReviewRepository implements BrandReviewRepositoryInterface
{
    /**
     * @var BrandReviewFactory
     */
    private $brandReviewFactory;

    /**
     * @var BrandReviewCollectionFactory
     */
    private $brandReviewCollectionFactory;

    /**
     * @var BrandReviewResource
     */
    private $resource;

    /**
     * @var JoinProcessorInterface
     */
    private $extensionAttributesJoinProcessor;

    /**
     * @var BrandReviewSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ExtensibleDataObjectConverter
     */
    private $extensibleDataObjectConverter;

    /**
     * @param BrandReviewFactory $brandReviewFactory
     * @param BrandReviewCollectionFactory $brandReviewCollectionFactory
     * @param BrandReviewResource $resource
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param BrandReviewSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        BrandReviewFactory $brandReviewFactory,
        BrandReviewCollectionFactory $brandReviewCollectionFactory,
        BrandReviewResource $resource,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        BrandReviewSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->brandReviewFactory = $brandReviewFactory;
        $this->brandReviewCollectionFactory = $brandReviewCollectionFactory;
        $this->resource = $resource;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * @param Data\BrandReviewInterface $brandReview
     * @return mixed
     * @throws CouldNotSaveException
     */
    public function save(Data\BrandReviewInterface $brandReview)
    {
        $brandReviewData = $this->extensibleDataObjectConverter->toNestedArray(
            $brandReview,
            [],
            \Aureatelabs\BrandReview\Api\Data\BrandReviewInterface::class
        );
        $brandReviewModel = $this->brandReviewFactory->create()->setData($brandReviewData);
        try {
            $brandReviewModel->setStatus(1);
            $this->resource->save($brandReviewModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Review: %1',
                $exception->getMessage()
            ));
        }
        return $brandReviewModel->getDataModel();
    }

    /**
     * @param $id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function get($id)
    {
        $brandReview = $this->brandReviewFactory->create();
        $this->resource->load($brandReview, $id);
        if (!$brandReview->getId()) {
            throw new NoSuchEntityException(__('Review with id "%1" does not exist.', $id));
        }
        return $brandReview->getDataModel();
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->brandReviewCollectionFactory->create();
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Aureatelabs\BrandReview\Api\Data\BrandReviewInterface::class
        );
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\BrandReviewInterface $brandReview
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\BrandReviewInterface $brandReview): bool
    {
        try {
            $brandReviewModel = $this->brandReviewFactory->create();
            $this->resource->load($brandReviewModel, $brandReview->getEntityId());
            $this->resource->delete($brandReviewModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Review: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id): bool
    {
        return $this->delete($this->get($id));
    }

}
