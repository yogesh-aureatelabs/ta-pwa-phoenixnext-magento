<?php

namespace Aureatelabs\SitemapExtend\Model\ItemProvider;

use Magento\Sitemap\Model\ResourceModel\Cms\PageFactory;
use Magento\Sitemap\Model\SitemapItemInterfaceFactory;
use Magento\Sitemap\Model\ItemProvider\ConfigReaderInterface;
use Aureatelabs\Blog\Model\ResourceModel\Post;
use Aureatelabs\Brands\Model\ResourceModel\Brand;

/**
 * Class CmsPage
 */
class CmsPage extends \Magento\Sitemap\Model\ItemProvider\CmsPage
{
    /**
     * Cms page factory
     *
     * @var PageFactory
     */
    private $cmsPageFactory;

    /**
     * Sitemap item factory
     *
     * @var SitemapItemInterfaceFactory
     */
    private $itemFactory;

    /**
     * Config reader
     *
     * @var ConfigReaderInterface
     */
    private $configReader;

    /**
     * Post connection
     *
     * @var Post
     */
    private $post;

    /**
     * Brand connection
     *
     * @var Brand
     */
    private $brand;

    /**
     * CmsPage constructor.
     *
     * @param ConfigReaderInterface       $configReader
     * @param PageFactory                 $cmsPageFactory
     * @param SitemapItemInterfaceFactory $itemFactory
     * @param Post $post
     * @param Brand $brand
     */
    public function __construct(
        ConfigReaderInterface $configReader,
        PageFactory $cmsPageFactory,
        SitemapItemInterfaceFactory $itemFactory,
        Post $post,
        Brand $brand
    ) {
        $this->cmsPageFactory = $cmsPageFactory;
        $this->itemFactory = $itemFactory;
        $this->configReader = $configReader;
        $this->post = $post;
        $this->brand = $brand;
    }

    /**
     * {@inheritdoc}
     */
    public function getItems($storeId)
    {
        $collection = $this->cmsPageFactory->create()->getCollection($storeId);
        $items = array_map(function ($item) use ($storeId) {
            return $this->itemFactory->create([
                'url' => $item->getUrl(),
                'updatedAt' => $item->getUpdatedAt(),
                'images' => $item->getImages(),
                'priority' => $this->configReader->getPriority($storeId),
                'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
            ]);
        }, $collection);

        $posts = $this->post->loadPosts($storeId);
        $postItems = array_map(function ($postItem) use ($storeId) {
            return $this->itemFactory->create([
                'url' => 'guild/' . $postItem['identifier'],
                'updatedAt' => $postItem['update_time'],
                'images' => null,
                'priority' => $this->configReader->getPriority($storeId),
                'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
            ]);
        }, $posts);

        $postItems[] = $this->itemFactory->create([
            'url' => 'guild',
            'updatedAt' => '',
            'images' => null,
            'priority' => $this->configReader->getPriority($storeId),
            'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
        ]);

        foreach ($postItems as $pitem) {
            array_push($items, $pitem);
        }

        $brands = $this->brand->loadBrands($storeId);
        $brandItems = array_map(function ($brandItem) use ($storeId) {
            return $this->itemFactory->create([
                'url' => 'series/' . $brandItem['link'],
                'updatedAt' => $brandItem['updated_at'],
                'images' => null,
                'priority' => $this->configReader->getPriority($storeId),
                'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
            ]);
        }, $brands);

        $brandItems[] = $this->itemFactory->create([
            'url' => 'series',
            'updatedAt' => '',
            'images' => null,
            'priority' => $this->configReader->getPriority($storeId),
            'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
        ]);

        foreach ($brandItems as $bitem) {
            array_push($items, $bitem);
        }

        return $items;
    }
}
