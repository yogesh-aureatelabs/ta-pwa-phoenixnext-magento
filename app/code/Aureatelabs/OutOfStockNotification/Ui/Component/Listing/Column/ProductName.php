<?php

namespace Aureatelabs\OutOfStockNotification\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Catalog\Model\ProductFactory;

class ProductName extends Column
{

    /**
     * 
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ProductFactory $product,
        array $components = [],
        array $data = []
    ) {
        $this->product = $product;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource) {
        $dataSource = parent::prepareDataSource($dataSource);
        if (empty($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as &$item) {
            if (isset($item['product_id']) && !empty($item['product_id'])) {
                $product = $this->getProduct($item['product_id']);
                $item['product_id'] = $product->getName();
            }
        }

        return $dataSource;
    }

    public function getProduct($id)
    {
        return $this->product->create()->load($id);
    }

}