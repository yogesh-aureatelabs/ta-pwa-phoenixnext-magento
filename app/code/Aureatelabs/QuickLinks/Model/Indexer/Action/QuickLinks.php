<?php

namespace Aureatelabs\QuickLinks\Model\Indexer\Action;

use Aureatelabs\QuickLinks\Model\ResourceModel\QuickLinks as QuickLinksResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

class QuickLinks implements RebuildActionInterface
{
    /**
     * @var QuickLinksResource
     */
    private $resourceModel;

    /**
     * @param QuickLinksResource $quickLinksResource
     */
    public function __construct(
        QuickLinksResource $quickLinksResource
    ) {
        $this->resourceModel = $quickLinksResource;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastLinkId = 0;
        do {
            $links = $this->resourceModel->loadLinks($ids, $lastLinkId);
            foreach ($links as $link) {
                $lastLinkId = $link['quicklinks_id'];
                $link['id'] = $link['quicklinks_id'];
                $link['include_in_menu'] = (bool) $link['include_in_menu'];
                $link['include_in_footer'] = (int) $link['include_in_footer'];

                yield $lastLinkId => $link;
            }

        } while (!empty($banners));
    }
}
