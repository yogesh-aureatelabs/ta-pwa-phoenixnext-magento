<?php
namespace Aureatelabs\QuickLinks\Block\Adminhtml\Items\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Main extends Generic implements TabInterface
{
    protected $_wysiwygConfig;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Item Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_aureatelabs_quicklinks_items');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('item_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Item Information')]);
        if ($model->getId()) {
            $fieldset->addField('quicklinks_id', 'hidden', ['name' => 'quicklinks_id']);
        }
        $fieldset->addField(
            'label',
            'text',
            ['name' => 'label', 'label' => __('Label'), 'title' => __('Label'), 'required' => true]
        );
        $fieldset->addField(
            'link',
            'text',
            ['name' => 'link', 'label' => __('Link'), 'title' => __('Link'), 'required' => true]
        );
        $fieldset->addField(
            'include_in_menu',
            'select',
            ['name' => 'include_in_menu', 'label' => __('Include in Menu'), 'title' => __('Include in Menu'),  'options'   => [0 => 'No', 1 => 'Yes'], 'required' => true]
        );
        $fieldset->addField(
            'include_in_footer',
            'select',
            ['name' => 'include_in_footer', 'label' => __('Include in Footer'), 'title' => __('Include in Footer'),  'options'   => [0 => 'No', 1 => 'Yes'], 'required' => true]
        );
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
