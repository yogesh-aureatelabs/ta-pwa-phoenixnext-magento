<?php

namespace Aureatelabs\SocialSignup\Api;
use Magento\Framework\Exception\AuthenticationException;

/**
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
interface SocialSignupInterface
{
    /**
     * Social sign up
     * 
     * @param int $customerId
     * @param string $facebookId
     * @param string $googleId
     * @return ResponseInterface
     * @throws AuthenticationException
     */
    public function signup(int $customerId, $facebookId = null, $googleId = null);

    /**
     * Social sign up
     * 
     * @param string $facebookId
     * @param string $googleId
     * @return string
     * @throws AuthenticationException
     */
    public function signin($facebookId = null, $googleId = null);

    /**
     * New sign up
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $googleId
     * @param string $facebookId
     * @return string
     * @throws AuthenticationException
     */
    public function newsignup($firstname, $lastname, $email, $type, $typeId);
}
