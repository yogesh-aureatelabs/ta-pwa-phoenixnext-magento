<?php

namespace Aureatelabs\Catalog\Model\Indexer\DataProvider;

use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;

class ProductStickerData implements DataProviderInterface
{

    private $ready = 11;

    private $preOrder = 12;

    private $eavConfig;

    public function __construct(\Magento\Eav\Model\Config $eavConfig)
    {
        $this->eavConfig = $eavConfig;
    }

    /**
     * @param array $indexData
     * @param $storeId
     * @return array
     */
    public function addData(array $indexData, $storeId): array
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'pre_stock_status');
        $options = $attribute->getSource()->getAllOptions();
        foreach($options as $option) {
            $label = $option['label'];
            $value = $option['value'];
            if (strtolower($label) == 'ready') {
                $this->ready = $value;
            }

            if (strtolower($label) == 'preorder') {
                $this->preOrder = $value;
            }
        }

        foreach ($indexData as &$product) {

            $oldStatus = isset($product['stock_status']) ? $product['stock_status']: 'Ready';
            $oldStatus = ($oldStatus == 'Ready') ? $this->ready : $this->preOrder;

            $product['pre_stock_status'] = (isset($product['pre_stock_status']) && !empty($product['pre_stock_status'])) ? $product['pre_stock_status'] : $oldStatus;
        }
        return $indexData;
    }
}
