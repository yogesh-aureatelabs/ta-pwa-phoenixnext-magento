<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Catalog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Catalog\Api;

/**
 * Catalog management interface.
 * @api
 */
interface CatalogManagementInterface
{
    /**
     * Save customer detail to notify them on availability of product's stock.
     *
     * @param \Aureatelabs\Catalog\Api\Data\NotifyInterface $notify
     * @return \Aureatelabs\Catalog\Api\CatalogResultInterface $catalogResult
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function notify(\Aureatelabs\Catalog\Api\Data\NotifyInterface $notify);
}
