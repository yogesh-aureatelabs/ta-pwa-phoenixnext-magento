<?php

namespace Aureatelabs\Catalog\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Review\Model\Review;

class Autoreview implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $review = $observer->getDataByKey('object');
        $review->setStatusId(Review::STATUS_APPROVED);
    }
}

