<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RewardsPoint\Model;

class GetRewardsHistory implements \Aureatelabs\RewardsPoint\Api\GetRewardsHistoryInterface
{

    /**
     * @var \Mirasvit\Rewards\Model\TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Mirasvit\Rewards\Helper\Purchase
     */
    private $rewardsPurchase;

    /**
     * @param \Mirasvit\Rewards\Model\TransactionFactory $transactionFactory
     * @param \Magento\Customer\Model\Customer           $customerRepository
     */
    public function __construct(
        \Mirasvit\Rewards\Model\TransactionFactory $transactionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMask,
        \Mirasvit\Rewards\Helper\Purchase $rewardsPurchase,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\App\RequestInterface $request,
        \Mirasvit\Rewards\Helper\Checkout $rewardsCheckout
    ) {
        $this->transactionFactory = $transactionFactory;
        $this->customerRepository = $customerRepository;
        $this->_quoteIdMask       = $quoteIdMask;
        $this->rewardsPurchase    = $rewardsPurchase;
        $this->quoteRepository    = $quoteRepository;
        $this->request            = $request;
        $this->rewardsCheckout    = $rewardsCheckout;
    }

    /**
     * {@inheritdoc}
     */
    public function getRewardsHistory($customer_id)
    {
        $collection = $this->transactionFactory->create()
            ->getCollection()
            ->addFieldToFilter('customer_id', $customer_id);

            $res = [];
            foreach ($collection as $rewardshistories) {
               $res[] = $rewardshistories->getData();
            }

        return $res;
    }

    /**
     * {@inheritdoc}
     */
    public function apply($cartId, $pointsAmount, $customerId)
    {
       // $cartId = $cartId;
        if(!is_numeric($cartId)){
            $quote_id_new = $this->_quoteIdMask->create()->load($cartId, 'masked_id');
            $cartId = $quote_id_new->getQuoteId();
        }
        $purchase = $this->rewardsPurchase->getByQuote($cartId);

        /* @var $quote \Magento\Quote\Model\Quote */
        $quote = $this->quoteRepository->getActive($cartId);

        $customer = $this->customerRepository->getById($customerId);
        $quote->assignCustomer($customer);
        $purchase->setQuote($quote);

        $this->request->setParams(['points_amount' => $pointsAmount]);

        return $this->rewardsCheckout->processApiRequest($purchase)['success'];
    }
}
