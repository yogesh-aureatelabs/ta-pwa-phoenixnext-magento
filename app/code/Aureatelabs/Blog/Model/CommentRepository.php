<?php

namespace Aureatelabs\Blog\Model;

use Aureatelabs\Blog\Api\CommentRepositoryInterface;
use Aureatelabs\Blog\Model\Comment;
use Magefan\Blog\Model\CommentFactory;
use Magefan\Blog\Model\ResourceModel\Comment as CommentResourceModel;
use Magefan\Blog\Model\ResourceModel\Comment\CollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsFactory;
use Magento\Framework\DB\Adapter\ConnectionException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\ValidatorException;

class CommentRepository extends \Magefan\Blog\Model\CommentRepository implements CommentRepositoryInterface
{
    /**
     * @var CommentResourceModel
     */
    private $commentResourceModel;

    /**
     * @param CommentFactory $commentFactory
     * @param CommentResourceModel $commentResourceModel
     * @param CollectionFactory $collectionFactory
     * @param SearchResultsFactory $searchResultsFactory
     * @param CollectionProcessorInterface|null $collectionProcessor
     */
    public function __construct(
        \Magefan\Blog\Model\CommentFactory $commentFactory,
        CommentResourceModel $commentResourceModel,
        CollectionFactory $collectionFactory,
        SearchResultsFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->commentResourceModel = $commentResourceModel;
        parent::__construct(
            $commentFactory,
            $commentResourceModel,
            $collectionFactory,
            $searchResultsFactory,
            $collectionProcessor
        );
    }

    /**
     * @param \Aureatelabs\Blog\Model\Comment $comment
     * @return bool
     * @throws AlreadyExistsException
     * @throws CouldNotSaveException
     */
    public function saveComment(Comment $comment): bool
    {
        if ($comment) {
            try {
                $comment->setStatus(1);
                $this->commentResourceModel->save($comment);
            } catch (ConnectionException $exception) {
                throw new CouldNotSaveException(
                    __('Database connection error'),
                    $exception,
                    $exception->getCode()
                );
            } catch (CouldNotSaveException $e) {
                throw new CouldNotSaveException(__('Unable to save item'), $e);
            } catch (ValidatorException $e) {
                throw new CouldNotSaveException(__($e->getMessage()));
            } catch (AlreadyExistsException $e) {
                throw new AlreadyExistsException(__($e->getMessage()));
            }
            return true;
        }
        return false;
    }
}
