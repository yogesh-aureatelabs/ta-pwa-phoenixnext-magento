<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Class Category
 * @package Aureatelabs\Blog\Model\ResourceModel
 */
class Category
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var MetadataPool
     */
    private $metaDataPool;

    /**
     * Category constructor.
     *
     * @param MetadataPool $metadataPool
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
        $this->metaDataPool = $metadataPool;
    }

    /**
     * @param int $storeId
     * @param array $categoryIds
     * @param int $fromId
     * @param int $limit
     *
     * @return array
     * @throws \Exception
     */
    public function loadCategories($storeId = 1, array $categoryIds = [], $fromId = 0, $limit = 1000)
    {
        $select = $this->getConnection()->select()->from([ 'category' => $this->resource->getTableName('magefan_blog_category')]);

        if (!empty($categoryIds)) {
            $select->where('category.category_id IN (?)', $categoryIds);
        }

        $select->where('category.is_active = ?', 1);

        $select->where('category.category_id > ?', $fromId)
            ->limit($limit)
            ->order('category.category_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getPosts($categoryId)
    {
        $options = [];

        $select = $this->getConnection()->select()->from([ 'category' => $this->resource->getTableName('magefan_blog_post_category')]);
        $select->where('category.category_id = ?', $categoryId);

        $records = $this->getConnection()->fetchAll($select);
        if(count($records)) {
            foreach ($records as $record) {
                $options[] = $record['post_id'];
            }
        }

        return $options;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resource->getConnection();
    }
}
