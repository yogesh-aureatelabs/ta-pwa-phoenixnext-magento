<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Class Post
 * @package Aureatelabs\Blog\Model\ResourceModel
 */
class Post
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var MetadataPool
     */
    private $metaDataPool;

    /**
     * Post constructor.
     *
     * @param MetadataPool $metadataPool
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection
    ) {
        $this->resource = $resourceConnection;
        $this->metaDataPool = $metadataPool;
    }

    /**
     * @param int $storeId
     * @param array $postIds
     * @param int $fromId
     * @param int $limit
     *
     * @return array
     * @throws \Exception
     */
    public function loadPosts($storeId = 1, array $postIds = [], $fromId = 0, $limit = 1000)
    {
        $select = $this->getConnection()->select()->from([ 'post' => $this->resource->getTableName('magefan_blog_post')]);

        if (!empty($postIds)) {
            $select->where('post.post_id IN (?)', $postIds);
        }

        $select->where('is_active = ?', 1);
        $select->where('post.post_id > ?', $fromId)
            ->limit($limit)
            ->order('post.post_id');

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * @param $postId
     * @param $storeId
     * @return array
     */
    public function loadBrandByTags($postId, $storeId)
    {
        $tag = [];
        $select = $this->getConnection()->select()->from([ 'post' => $this->resource->getTableName('magefan_blog_post_tag')]);
        $select->where('post.post_id = ?', $postId);

        $table = $this->resource->getTableName('magefan_blog_tag');
        $select->joinLeft("$table AS tag", "tag.tag_id = post.tag_id", ['tag.title']);

        $tags = [];
        $records = $this->getConnection()->fetchAll($select);
        if(count($records)) {
            foreach ($records as $record) {
                $tags[] = $record['title'];
            }
        }
        if (count($tags)) {
            foreach ($tags as $record) {
                $tag[] = $record;
            }
        }
        return $tag;
    }

    /**
     * @param $postId
     * @return array
     */
    public function loadRelatedPosts($postId)
    {
        $posts = [];

        $select = $this->getConnection()->select()->from([ 'post' => $this->resource->getTableName('magefan_blog_post_relatedpost')], ['related_id']);
        $select->where('post.post_id = ?', $postId);

        $records = $this->getConnection()->fetchAll($select);
        if(count($records)) {
            foreach ($records as $record) {
                $posts[] = $record['related_id'];
            }
        }

        return $posts;
    }

    /**
     * @param $postId
     * @return array
     */
    public function loadRelatedProducts($postId)
    {
        $posts = [];
        $select = $this->getConnection()->select()->from([ 'post' => $this->resource->getTableName('magefan_blog_post_relatedproduct')]);
        $select->where('post.post_id = ?', $postId);
        $records = $this->getConnection()->fetchAll($select);
        if(count($records)) {
            foreach ($records as $record) {
                $posts[] = $record['related_id'];
            }
        }
        return $posts;
    }

    /**
     * @param $postId
     * @return array
     */
    public function loadRelatedBrands($postId)
    {
        $posts = [];
        $select = $this->getConnection()->select()
            ->from([ 'brand' => $this->resource->getTableName('magefan_blog_post_relatedbrand')])
            ->join(
                ['ab' => $this->resource->getTableName('aureate_brands')],
                'ab.brand_id = brand.related_id',
                ['brand_name', 'brand_image', 'link']
            );
        $select->where('brand.post_id = ?', $postId);
        $records = $this->getConnection()->fetchAll($select);
        if(count($records)) {
            foreach ($records as $record) {
                $posts[] = $record;
            }
        }
        return $posts;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resource->getConnection();
    }
}
