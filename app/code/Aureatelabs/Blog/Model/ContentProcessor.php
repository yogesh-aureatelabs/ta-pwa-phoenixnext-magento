<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Model;

use Aureatelabs\Blog\Api\ContentProcessorInterface;
use Magento\Framework\Filter\Template as TemplateFilter;

/**
 * Class ContentProcessor
 * @package Aureatelabs\Blog\Model
 */
class ContentProcessor implements ContentProcessorInterface
{
    /**
     * @inheritdoc
     */
    public function parse(TemplateFilter $templateFilter, string $content)
    {
        return $templateFilter->filter($content);
    }
}
