<?php

namespace Aureatelabs\Blog\Model\Indexer\DataProvider;

use Aureatelabs\Blog\Model\ResourceModel\Post as PostResource;
use Divante\VsbridgeIndexerCore\Api\DataProviderInterface;
use Magefan\Blog\Model\ResourceModel\Author\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Cms\Model\Template\FilterProvider;

class Blog implements DataProviderInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var FilterProvider
     */
    private $filterProvider;

    /**
     * @var PostResource
     */
    private $resourceModel;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param PostResource $postResource
     * @param StoreManagerInterface $storeManager
     * @param FilterProvider $filterProvider
     * @param CollectionFactory $collectionFactory
     * @param Json $json
     */
    public function __construct(
        PostResource $postResource,
        StoreManagerInterface $storeManager,
        FilterProvider $filterProvider,
        CollectionFactory $collectionFactory,
        Json $json
    ) {
        $this->resourceModel = $postResource;
        $this->storeManager = $storeManager;
        $this->filterProvider = $filterProvider;
        $this->collectionFactory = $collectionFactory;
        $this->json = $json;
    }

    /**
     * @param array $indexData
     * @param int $storeId
     * @return array
     * @throws NoSuchEntityException
     */
    public function addData(array $indexData, $storeId)
    {
        $filter = $this->filterProvider->getBlockFilter();
        $mediaPath = $this->storeManager
            ->getStore()
            ->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            );
        $authors = $this->getAuthorFullName();
        foreach ($indexData as $post) {
            $post['content'] = $filter->filter($post['content']);
            if (!empty($post['featured_img'])) {
                $post['featured_img'] = $mediaPath . $post['featured_img'];
            }
            $gallery =  $post['media_gallery'];
            if(!empty($gallery)) {
                $gallery = explode(\Magefan\Blog\Model\Post::GALLERY_IMAGES_SEPARATOR, $post['media_gallery']);
                foreach ($gallery as $image) {
                    $gallery[] = $mediaPath . $image;
                }
                $post['media'] = $this->json->serialize($gallery);
                $post['author_name'] = $authors[$post['author_id']] ?? null;
                $post['tags'] = $this->resourceModel->loadBrandByTags($post['post_id'], $storeId);
                $post['relatedpost'] = $this->resourceModel->loadRelatedPosts($post['post_id']);
                $post['relatedproduct'] = $this->resourceModel->loadRelatedProducts($post['post_id']);
                $post['relatedbrand'] = $this->resourceModel->loadRelatedBrands($post['post_id']);
            }
        }
        return $indexData;
    }

    /**
     * @return array
     */
    private function getAuthorFullName(): array
    {
        $authors = $this->collectionFactory->create();
        foreach ($authors as $author) {
            $authorById[$author->getId()] = $author->getFirstname() . ' ' .  $author->getLastname();
        }
        return $authorById ?? [];
    }
}
