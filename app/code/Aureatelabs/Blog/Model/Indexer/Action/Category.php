<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Model\Indexer\Action;

use Aureatelabs\Blog\Model\ResourceModel\Category as CategoryResource;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

/**
 * Class Category
 * @package Aureatelabs\Blog\Model\Indexer\Action
 */
class Category implements RebuildActionInterface
{
    /**
     * @var CategoryResource
     */
    private $resourceModel;

    /**
     * Category constructor.
     * @param CategoryResource $categoryResource
     */
    public function __construct(
        CategoryResource $categoryResource
    ) {
        $this->resourceModel = $categoryResource;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     * @throws \Exception
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
        $lastCategoryId = 0;
        do {
            $categories = $this->resourceModel->loadCategories($storeId, $ids, $lastCategoryId);
            foreach ($categories as $category) {
                $lastCategoryId = $category['category_id'];
                $category['posts'] = $this->resourceModel->getPosts($category['category_id']);
                yield $lastCategoryId => $category;
            }
        } while (!empty($categories));
    }
}
