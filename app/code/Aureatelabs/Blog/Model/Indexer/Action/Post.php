<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Model\Indexer\Action;

use Aureatelabs\Blog\Model\ResourceModel\Post as PostResource;
use Magento\Framework\App\Area;
use Magento\Framework\App\AreaList;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Cms\Model\Template\FilterProvider;
use Aureatelabs\Blog\Api\ContentProcessorInterface;
use Divante\VsbridgeIndexerCore\Indexer\RebuildActionInterface;

/**
 * Class Post
 * @package Aureatelabs\Blog\Model\Indexer\Action
 */
class Post implements RebuildActionInterface
{
    /**
     * @var PostResource
     */
    private $resourceModel;

     /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ContentProcessorInterface
     */
    private $contentProcessor;

    /**
     * @var FilterProvider
     */
    private $filterProvider;

    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @param PostResource $postResource
     */
    public function __construct(
        PostResource $postResource,
        AreaList $areaList,
        StoreManagerInterface $storeManager,
        ContentProcessorInterface $contentProcessor,
        FilterProvider $filterProvider,
        \Magefan\Blog\Api\AuthorInterfaceFactory $authorFactory
    ) {
	$this->resourceModel = $postResource;
	$this->areaList = $areaList;
	$this->storeManager = $storeManager;
        $this->contentProcessor = $contentProcessor;
        $this->filterProvider = $filterProvider;
        $this->authorFactory = $authorFactory;
    }

    /**
     * @param int $storeId
     * @param array $ids
     * @return \Traversable
     * @throws \Exception
     */
    public function rebuild(int $storeId, array $ids): \Traversable
    {
	$this->areaList->getArea(Area::AREA_FRONTEND)->load(Area::PART_DESIGN);
	$lastPostId = 0;
	$filter = $this->filterProvider->getBlockFilter()->setStoreId($storeId);

        do {
            $posts = $this->resourceModel->loadPosts($storeId, $ids, $lastPostId);
	    foreach ($posts as $post) {

		$post['content'] = $this->contentProcessor->parse($filter, $post['content']);
                $post['featured_img'] = $this->storeManager
                        ->getStore()
                        ->getBaseUrl(
                            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                        ) . $post['featured_img'];
                      $post['creation_time_timestamp'] = strtotime($post['creation_time']);

                $gallery =  $post['media_gallery'];
                if(!empty($gallery)) {
                    $gallery = explode(\Magefan\Blog\Model\Post::GALLERY_IMAGES_SEPARATOR, $post['media_gallery']);
                    foreach ($gallery as $i => $image) {
                        $gallery[$i] = $this->storeManager
                                ->getStore()
                                ->getBaseUrl(
                                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                                ) . $image;
                    }

                    $post['media'] = json_encode($gallery);
                }
                $authorData = $this->authorFactory->create()->load($post['author_id']);
                $name =  $authorData->getFirstname() . ' ' .  $authorData->getLastname();
                $post['author_name'] = $name;
                $post['tags'] = $this->resourceModel->loadBrandByTags($post['post_id'], $storeId);
                $post['relatedpost'] = $this->resourceModel->loadRelatedPosts($post['post_id']);
                $post['relatedproduct'] = $this->resourceModel->loadRelatedProducts($post['post_id']);
                $post['relatedbrand'] = $this->resourceModel->loadRelatedBrands($post['post_id']);

                $lastPostId = $post['post_id'];
                $post['id'] = $post['post_id'];

                unset($post['is_active']);
                yield $lastPostId => $post;
            }
        } while (!empty($posts));
    }
}
