<?php

namespace Aureatelabs\Blog\Model\Indexer;

class CommentProcessor extends \Magento\Framework\Indexer\AbstractProcessor
{
    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_post_comment_indexer';
}
