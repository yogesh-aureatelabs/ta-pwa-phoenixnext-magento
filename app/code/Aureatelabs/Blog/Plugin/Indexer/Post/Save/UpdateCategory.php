<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Plugin\Indexer\Post\Save;

use Aureatelabs\Blog\Model\Indexer\CategoryProcessor;
use Magefan\Blog\Model\Category;

/**
 * Class UpdateCategory
 * @package Aureatelabs\Blog\Plugin\Indexer\Brand\Save
 */
class UpdateCategory
{
    /**
     * @var CategoryProcessor
     */
    private $categoryProcessor;

    /**
     * UpdateBrand constructor.
     *
     * @param CategoryProcessor $categoryProcessor
     */
    public function __construct(CategoryProcessor $categoryProcessor)
    {
        $this->categoryProcessor = $categoryProcessor;
    }

    /**
     * @param Category $category
     * @param Category $result
     *
     * @return Category
     */
    public function afterAfterSave(Category $category, Category $result)
    {
        $result->getResource()->addCommitCallback(function () use ($category) {
            $this->categoryProcessor->reindexRow($category->getId());
        });

        return $result;
    }

    /**
     * @param Category $category
     * @param Category $result
     *
     * @return Category
     */
    public function afterAfterDeleteCommit(Category $category, Category $result)
    {
        $this->categoryProcessor->reindexRow($category->getId());

        return $result;
    }
}
