<?php
namespace Aureatelabs\Blog\Plugin\Block\Adminhtml\System\Config\Form;

class RemoveNotice
{
    /**
     * @return array|bool
     */
    public function afterIsAnotherBlogModulesEnabled(\Magefan\Blog\Block\Adminhtml\System\Config\Form\CheckEnableInfo $subject, $result)
    {
        return false;
    }
}
