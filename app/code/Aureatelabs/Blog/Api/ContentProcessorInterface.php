<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Blog
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Blog\Api;

use Magento\Framework\Filter\Template as TemplateFilter;

/**
 * Interface ContentProcessorInterface
 * @package Aureatelabs\Blog\Api
 */
interface ContentProcessorInterface
{
    /**
     * @param TemplateFilter $templateFilter
     * @param string $content
     *
     * @return string|array
     * @throws \Exception
     */
    public function parse(TemplateFilter $templateFilter, string $content);
}
