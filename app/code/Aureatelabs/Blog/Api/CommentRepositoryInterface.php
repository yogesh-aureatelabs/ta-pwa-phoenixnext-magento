<?php

namespace Aureatelabs\Blog\Api;

use Aureatelabs\Blog\Model\Comment;

interface CommentRepositoryInterface extends \Magefan\Blog\Api\CommentRepositoryInterface
{
    /**
     * @param Comment $comment
     * @return bool
     */
    public function saveComment(Comment $comment): bool;
}
