<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Contact
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Contact\Model;

use Aureatelabs\Contact\Api\ContactInterface;
use Magento\Contact\Model\MailInterface;
use Magento\Framework\DataObject;
use Aureatelabs\Contact\Api\ResultInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Contact
 * @package Aureatelabs\Contact\Model
 */
class Contact implements ContactInterface
{

    /**
     * @var MailInterface
     */
    private $mail;

    /**
     * @var ResultInterface
     */
    private $_result;

    /**
     * Contact constructor.
     *
     * @param MailInterface $mail
     * @param ResultInterface $result
     */
    public function __construct(
        MailInterface $mail,
        ResultInterface $result
    ) {

        $this->mail = $mail;
        $this->_result = $result;
    }

    /**
     * Returns greeting message to user
     *
     * @param \Aureatelabs\Contact\Api\Data\SubscriberInterface $subscriber
     * @return \Aureatelabs\Contact\Api\ResultInterface
     */
    public function post($subscriber)
    {
        $this->_result->setCode(400)->setMessage('Error');
        try {

            $this->sendEmail($this->validatedParams($subscriber));
            $message = __('Thanks for contacting us with your comments and questions. We\'ll respond to you very soon.');
            $this->_result->setCode(200)->setMessage('Success');

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        $this->_result->setResult($message);
        return $this->_result;
    }

    /**
     * @param array $post Post data from contact form
     * @return void
     */
    private function sendEmail($post)
    {
        $this->mail->send(
            $post['email'],
            ['data' => new DataObject($post)]
        );
    }

    /**
     * @param \Aureatelabs\Contact\Api\Data\SubscriberInterface $subscriber
     * @return array
     * @throws LocalizedException
     */
    private function validatedParams($subscriber)
    {
        if (trim($subscriber->getName()) === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if (trim($subscriber->getComment()) === '') {
            throw new LocalizedException(__('Enter the comment and try again.'));
        }
        if (false === \strpos($subscriber->getEmail(), '@')) {
            throw new LocalizedException(__('The email address is invalid. Verify the email address and try again.'));
        }

        return $subscriber->getData();
    }
}
