<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\CustomerApi\Api\Data;

interface CustomersSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Customers list.
     * @return \Aureatelabs\CustomerApi\Api\Data\CustomersInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \Aureatelabs\CustomerApi\Api\Data\CustomersInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

