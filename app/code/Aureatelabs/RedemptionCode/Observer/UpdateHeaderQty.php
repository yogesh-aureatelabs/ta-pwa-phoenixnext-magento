<?php
namespace Aureatelabs\RedemptionCode\Observer;

use Magento\Framework\Event\ObserverInterface;
use Aureatelabs\RedemptionCode\Model\RedemptionCodeProductFactory;
use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct\CollectionFactory;
use Psr\Log\LoggerInterface;

class UpdateHeaderQty implements ObserverInterface
{
    public function __construct(
        LoggerInterface $loggerInterface,
        RedemptionCodeProductFactory $redemptionCodeProductFactory,
        CollectionFactory $collectionFactory,
        \Aureatelabs\RedemptionCode\Model\RedemptionCodeHeaderFactory $redemptionCodeHeaderFactory
    ) {
        $this->logLoggerInterface = $loggerInterface;
        $this->redemptionCodeProductFactory = $redemptionCodeProductFactory;
        $this->redemptionCodeHeaderFactory = $redemptionCodeHeaderFactory;
        $this->collectionFactory = $collectionFactory;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        
        try {
            // Get the model object from observer
            $model = $observer->getEvent()->getDataObject();

            $codeId = $model->getId();
            $headerId = $model->getRedemptionCodeHeaderId();
            
            $codeCollection = $this->collectionFactory->create()
                                ->addFieldToFilter('redemption_code_header_id', ['eq' => $headerId])
                                ->addFieldToFilter('is_purchased',['neq' => 1]);
            $qtyNotPurchased = $codeCollection->count();

            $headerModel = $this->redemptionCodeHeaderFactory->create()->load($headerId);
            $headerModel->setData('qty_in_stock', $qtyNotPurchased);
            $headerModel->save();
            
        } catch (\Exception $e) {
        }
    }
}