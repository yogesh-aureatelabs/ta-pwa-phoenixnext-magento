<?php

namespace Aureatelabs\RedemptionCode\Observer;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\Observer;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Psr\Log\LoggerInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\ResponseFactory;

/**
 * Import
 */
class Import implements \Magento\Framework\Event\ObserverInterface
{
    protected $rcp;
    public $resourceConnection;
    public $directorylist;
    public $cacheTypeList;
    public $cacheFrontendPool;
    protected $logger;
    protected $_url;
    protected $messageManager;
    protected $_responseFactory;

    const ITEM_TABLE = 'redemption_code_item';
    const ITEM_HEADER_TABLE = 'redemption_code_header';
    const CONFIG_TABLE = 'core_config_data';

    /**
     * __construct
     *
     * @param  ResourceConnection $resourceConnection
     * @param  DirectoryList $directorylist
     * @param  Pool $cacheFrontendPool,
     * @param  TypeListInterface $cacheTypeList,
     * @param  LoggerInterface $logger,
     * @param  ManagerInterface $messageManager
     * @param  UrlInterface $url
     * @param  ResponseFactory $responseFactory
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        DirectoryList $directorylist,
        Pool $cacheFrontendPool,
        TypeListInterface $cacheTypeList,
        LoggerInterface $logger,
        ManagerInterface $messageManager,
        UrlInterface $url,
        ResponseFactory $responseFactory
    ) {
        $this->messageManager = $messageManager;
        $this->cacheFrontendPool = $cacheFrontendPool;
        $this->cacheTypeList = $cacheTypeList;
        $this->resourceConnection = $resourceConnection;
        $this->directorylist = $directorylist;
        $this->logger = $logger;
        $this->_url = $url;
        $this->_responseFactory = $responseFactory;
    }

    /**
     * execute
     *
     * @param  Observer $observer
     */
    public function execute(Observer $observer)
    {
        set_time_limit(0);
        $path = $this->directorylist->getPath('media');
        $scandir = scandir($path . "/redemptioncodeimport/");
        $errorMsg = '';
        $updateHeaderIds = [];
        if (isset($scandir[2])) {
            $firstFile = scandir($path . "/redemptioncodeimport/")[2];
            $filepath = $path . '/redemptioncodeimport/' . $firstFile;

            if (file_exists($filepath)) {
                $file = fopen($filepath, 'r', '"'); // set path to the CSV file

                if ($file !== false) {
                    $flag = true;
                    $rowNumber = 2;
                    $data = $fullData = [];

                    while (($row = fgetcsv($file, 5000, ",")) !== FALSE) {
                        if ($flag) {
                            $flag = false;
                            continue;
                        }

                        $connection  = $this->resourceConnection->getConnection();
                        $itemTableName = $connection->getTableName(self::ITEM_TABLE);
                        $itemQuery = $connection->select()->from($itemTableName, ['id'])->where('redemption_code = ?', $row[1]);
                        $itemId = $connection->fetchOne($itemQuery);

                        if (!$itemId) {
                            $purchased = strtolower($row[3]) == 'yes' ? 1 : 0;

                            if ($purchased == 1) {
                                $entry = 1;
                                $orderId = trim(str_replace("'", "", $row[4]));
                                if ($orderId == '') {
                                    $entry = 0;
                                    $errorMsg = $errorMsg . '<p>Order ID required at row: ' . $rowNumber . '</p>';
                                }

                                if ($row[5] == '') {
                                    $entry = 0;
                                    $errorMsg = $errorMsg . '<p>Email address required at row: ' . $rowNumber . '</p>';
                                }

                                if ($row[6] == '') {
                                    $entry = 0;
                                    $errorMsg = $errorMsg . '<p>Email send at required at row: ' . $rowNumber . '</p>';
                                }

                                if ($entry == 1) {
                                    $fullData[] = [
                                        'redemption_code_item_number' => $row[0],
                                        'redemption_code' => $row[1],
                                        'redemption_code_header_id' => $row[2],
                                        // 'is_reserved' => 0,
                                        'is_purchased' => $purchased,
                                        'order_id' => (string)$orderId,
                                        'email' => (string)$row[5],
                                        'email_sent_timestamp' => date("Y-m-d H:i:s", strtotime($row[6]))
                                    ];
                                    if (!in_array($row[2], $updateHeaderIds)){
                                        $updateHeaderIds[]=$row[2];
                                    }
                                }
                            } else {
                                $data[] = [
                                    'redemption_code_item_number' => $row[0],
                                    'redemption_code' => $row[1],
                                    'redemption_code_header_id' => $row[2],
                                    // 'is_reserved' => 0,
                                    'is_purchased' => $purchased,
                                ];
                                if (!in_array($row[2], $updateHeaderIds)){
                                    $updateHeaderIds[]=$row[2];
                                }
                            }
                        }

                        $rowNumber++;
                    }

                    $this->logger->info(json_encode($data));

                    if (!empty($data)) {
                        try {
                            $conn  = $this->resourceConnection->getConnection();
                            $itemTblName = $conn->getTableName(self::ITEM_TABLE);
                            $conn->insertMultiple($itemTblName, $data);
                            if(!empty($updateHeaderIds)){
                                foreach($updateHeaderIds as $headerId){
                                    $itemQuery = $conn->select()->from($itemTblName, ['id'])->where('redemption_code_header_id = '.$headerId.' and is_purchased = 0');
                                    $data = $conn->fetchAll($itemQuery);
                                    $count = count($data);
                                    $data = ["qty_in_stock" => $count];
                                    $where = ['id = ?' => (int)$headerId];
                                    $HeaderTblName = $conn->getTableName(self::ITEM_HEADER_TABLE);
                                    $conn->update($HeaderTblName, $data, $where);
                                }   
                            }
                        } catch (\Exception $e) {
                            $errorMsg = $errorMsg . '<p>Error importing redemption codes: ' . $e->getMessage() . '</p>';
                            $this->logger->info('Error importing redemption codes: ' . $e->getMessage());
                        }
                    }

                    if (!empty($fullData)) {
                        try {
                            $conn  = $this->resourceConnection->getConnection();
                            $itemTblName = $conn->getTableName(self::ITEM_TABLE);
                            $conn->insertMultiple($itemTblName, $fullData);
                            if(!empty($updateHeaderIds)){
                                foreach($updateHeaderIds as $headerId){
                                    $itemQuery = $conn->select()->from($itemTblName, ['id'])->where('redemption_code_header_id = '.$headerId.' and is_purchased = 0');
                                    $count = count($conn->fetchAll($itemQuery));
                                    $data = $conn->fetchAll($itemQuery);
                                    $count = count($data);
                                    $data = ["qty_in_stock" => $count];
                                    $where = ['id = ?' => (int)$headerId];
                                    $HeaderTblName = $conn->getTableName(self::ITEM_HEADER_TABLE);
                                    $conn->update($HeaderTblName, $data, $where);
                                }   
                            }
                        } catch (\Exception $e) {
                            $errorMsg = $errorMsg . '<p>Error importing purchased redemption codes: ' . $e->getMessage() . '</p>';
                            $this->logger->info('Error importing purchased redemption codes: ' . $e->getMessage());
                        }
                    }
                }
            }

            fclose($file);
            unlink($filepath);
        }

        $csvData = $data + $fullData;
        $this->updateConfig();
        $message = !empty($csvData) ? "CSV import successful." : 'No new data to import';
        $this->messageManager->addSuccess($message);
        if ($errorMsg != '') {
            $this->messageManager->addError($errorMsg);
        }
        $redirectUrl = $this->_url->getUrl('adminhtml/system_config/edit/section/redemptioncodeimport');
        $this->_responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
        exit;
    }

    /**
     * updateConfig
     */
    public function updateConfig()
    {
        $connection  = $this->resourceConnection->getConnection();
        $configTableName = $connection->getTableName(self::CONFIG_TABLE);
        $sql = "Update " . $configTableName . " SET value = NULL where path = 'redemptioncodeimport/custom_section/redemptioncodeimport_file_upload'";
        $connection->query($sql);
        return true;
    }
}
