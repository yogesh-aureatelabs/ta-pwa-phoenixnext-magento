<?php

namespace Aureatelabs\RedemptionCode\Observer;

use Aureatelabs\RedemptionCode\Model\RedemptionCodeProductFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class AfterPlaceOrder implements ObserverInterface
{
    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var LoggerInterface
     */
    protected $logLoggerInterface;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var RedemptionCodeProductFactory
     */
    protected $redemptionCodeProductFactory;

    /**
     * AfterPlaceOrder constructor.
     * @param StateInterface $inlineTranslation
     * @param TransportBuilder $transportBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param LoggerInterface $loggerInterface
     * @param StoreManagerInterface $storeManager
     * @param RedemptionCodeProductFactory $redemptionCodeProductFactory
     * @param TimezoneInterface $timezoneInterface
     */
    public function __construct(
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $loggerInterface,
        StoreManagerInterface $storeManager,
        RedemptionCodeProductFactory $redemptionCodeProductFactory,
        TimezoneInterface $timezoneInterface
    ) {
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->logLoggerInterface = $loggerInterface;
        $this->storeManager = $storeManager;
        $this->redemptionCodeProductFactory = $redemptionCodeProductFactory;
        $this->timezoneInterface = $timezoneInterface;
    }

    public function execute(Observer $observer)
    {
        // $order = $observer->getEvent()->getOrder();
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        try {
            $this->_inlineTranslation->suspend();
            $sender = [
                'email' => $this->_scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE),
                'name' =>  $this->_scopeConfig->getValue('trans_email/ident_general/name', ScopeInterface::SCOPE_STORE)
            ];

            $redemptionCode = "";
            foreach ($order->getAllVisibleItems() as $_item) {
                $redemptionCodeId = "";
                if (!$_item->getProduct()->getData('is_redemption_code')) {
                    continue;
                }

                $itemsCoupons = [];
                $headerIds = explode(",", $_item->getProduct()->getData('header_id'));
                $qty = $_item->getQtyOrdered();
                foreach ($headerIds as $value) {
                    $collection = $this->redemptionCodeProductFactory->create();
                    $itemCollection = $collection->getCollection()
                        ->addFieldToFilter("redemption_code_header_id", $value)
                        ->addFieldToFilter("is_purchased", 0);

                    if ($itemCollection->getSize() > 0) {
                        foreach ($itemCollection as $_collection) {
                            if ($_collection->getId() == $redemptionCodeId) {
                                continue;
                            }
                            // if (in_array($_collection->getId(), $redemptionCodeId)) {
                            //     continue;
                            // }
                            $redemptionCode = $_collection->getRedemptionCode();
                            $itemsCoupons[] = $_item->getName() . " - " . $redemptionCode;
                            $redemptionCodeId = $_collection->getId();
                            $qty--;
                            if ($qty == 0) {
                                break;
                            }
                        }
                    }
                }

                //$itemsCoupons[] = $_item->getName() ." - ". $redemptionCode;

                if ($redemptionCode && $redemptionCode != "") {
                    $to = $order->getCustomerEmail();
                    $transport = $this->_transportBuilder
                        ->setTemplateIdentifier('redemption_coupons')
                        ->setTemplateOptions(
                            [
                                'area' => 'frontend',
                                'store' => $this->storeManager->getStore()->getId()
                            ]
                        )
                        ->setTemplateVars([
                            'coupons' => $itemsCoupons,
                            'order_id' => $order->getIncrementId(),
                            'product_name' => $_item->getName(),
                            'product_qty' => $_item->getQtyOrdered(),
                            'customer_name' => $order->getCustomerName()
                        ])
                        ->setFromByScope($sender)
                        ->addTo($to)
                        ->getTransport();

                    $transport->sendMessage();
                    $this->_inlineTranslation->resume();
                    if ($redemptionCodeId) {
                        // foreach ($redemptionCodeId as $id) {
                            $dateTime = $this->timezoneInterface->date()->format('Y-m-d H:i:s');
                            
                            $collection = $this->redemptionCodeProductFactory->create();
                            $modal = $collection->load($redemptionCodeId);
                            $modal->setOrderId($order->getIncrementId());
                            $modal->setSku($_item->getSku());
                            $modal->setEmail($order->getCustomerEmail());
                            $modal->setIsPurchased(1);
                            $modal->setEmailSentTimestamp($dateTime);
                            $modal->save();
                        // }
                    }
                }
            }

        } catch (\Exception $e) {
            $this->logLoggerInterface->debug($e->getMessage());
            exit;
        }
    }
}
