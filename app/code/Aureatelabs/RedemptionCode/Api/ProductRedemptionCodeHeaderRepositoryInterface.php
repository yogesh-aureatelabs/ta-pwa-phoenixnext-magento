<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductRedemptionCodeHeaderRepositoryInterface
{

    /**
     * Save ProductRedemptionCodeHeader
     * @param \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface $productRedemptionCodeHeader
     * @return \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface $productRedemptionCodeHeader
    );

    /**
     * Retrieve ProductRedemptionCodeHeader
     * @param string $productredemptioncodeheaderId
     * @return \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($productredemptioncodeheaderId);

    /**
     * Retrieve ProductRedemptionCodeHeader matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ProductRedemptionCodeHeader
     * @param \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface $productRedemptionCodeHeader
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface $productRedemptionCodeHeader
    );

    /**
     * Delete ProductRedemptionCodeHeader by ID
     * @param string $productredemptioncodeheaderId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($productredemptioncodeheaderId);
}
