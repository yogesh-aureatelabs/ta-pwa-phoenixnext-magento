<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RedemptionCodeHeaderRepositoryInterface
{

    /**
     * Save RedemptionCodeHeader
     * @param \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface $redemptionCodeHeader
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface $redemptionCodeHeader
    );

    /**
     * Retrieve RedemptionCodeHeader
     * @param string $redemptioncodeheaderId
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($redemptioncodeheaderId);

    /**
     * Retrieve RedemptionCodeHeader matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete RedemptionCodeHeader
     * @param \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface $redemptionCodeHeader
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface $redemptionCodeHeader
    );

    /**
     * Delete RedemptionCodeHeader by ID
     * @param string $redemptioncodeheaderId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($redemptioncodeheaderId);
}
