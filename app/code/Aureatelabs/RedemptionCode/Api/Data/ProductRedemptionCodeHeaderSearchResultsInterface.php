<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api\Data;

interface ProductRedemptionCodeHeaderSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get ProductRedemptionCodeHeader list.
     * @return \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Aureatelabs\RedemptionCode\Api\Data\ProductRedemptionCodeHeaderInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
