<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api\Data;

interface RedemptionCodeProductSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get RedemptionCodeProduct list.
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

