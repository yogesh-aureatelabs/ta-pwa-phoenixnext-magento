<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api\Data;

interface RedemptionCodeProductInterface
{

    const REDEMPTIONCODEPRODUCT_ID = 'id';
    const REDEMPTION_CODE_HEADER_ID = 'redemption_code_header_id';
    const ORDER_ID = 'order_id';
    const UPDATED_TIMESTAMP = 'updated_timestamp';
    const REDEMPTION_CODE = 'redemption_code';
    const ID = 'id';
    const CREATED_TIMESTAMP = 'created_timestamp';
    const REDEMPTION_CODE_ITEM_NUMBER = 'redemption_code_item_number';
    const SKU = 'sku';
    const EMAIL = 'email';
    const IS_PURCHASED = 'is_purchased';

    /**
     * Get redemptioncodeproduct_id
     * @return string|null
     */
    public function getRedemptioncodeproductId();

    /**
     * Set redemptioncodeproduct_id
     * @param string $redemptioncodeproductId
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setRedemptioncodeproductId($redemptioncodeproductId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setId($id);

    /**
     * Get redemption_code
     * @return string|null
     */
    public function getRedemptionCode();

    /**
     * Set redemption_code
     * @param string $redemptionCode
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setRedemptionCode($redemptionCode);

    /**
     * Get redemption_code_item_number
     * @return string|null
     */
    public function getRedemptionCodeItemNumber();

    /**
     * Set redemption_code_header_id
     * @param string $redemptionCode
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setRedemptionCodeItemNumber($redemptionCodeItemNumber);

    /**
     * Get redemption_code_item_number
     * @return string|null
     */
    public function getRedemptionCodeHeaderId();

    /**
     * Set redemption_code_header_id
     * @param string $redemptionCode
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setRedemptionCodeHeaderId($redemptionCodeHeaderId);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setOrderId($orderId);

    /**
     * Get created_timestamp
     * @return string|null
     */
    public function getCreatedTimestamp();

    /**
     * Set created_timestamp
     * @param string $createdTimestamp
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setCreatedTimestamp($createdTimestamp);

    /**
     * Get updated_timestamp
     * @return string|null
     */
    public function getUpdatedTimestamp();

    /**
     * Set updated_timestamp
     * @param string $updatedTimestamp
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setUpdatedTimestamp($updatedTimestamp);

    /**
     * @return mixed
     */
    public function getSku();

    /**
     * @param $sku
     * @return mixed
     */
    public function setSku($sku);

    /**
     * @return mixed
     */
    public function getEmail();

    /**
     * @param $email
     * @return mixed
     */
    public function setEmail($email);

    /**
     * Get is_purchased
     * @return string|null
     */
    public function getIsPurchased();

    /**
     * Set is_purchased
     * @param string $updatedTimestamp
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeProduct\Api\Data\RedemptionCodeProductInterface
     */
    public function setIsPurchased($isPurchased);
}
