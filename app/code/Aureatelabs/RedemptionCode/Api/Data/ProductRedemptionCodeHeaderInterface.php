<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api\Data;

interface ProductRedemptionCodeHeaderInterface
{

    const REDEMPTION_CODE_HEADER_ID = 'redemption_code_header_id';
    const UPDATED_TIMESTAMP = 'updated_timestamp';
    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const PRODUCTREDEMPTIONCODEHEADER_ID = 'id';
    const CREATED_TIMESTAMP = 'created_timestamp';

    /**
     * Get id
     * @return string|null
     */
    public function getProductredemptioncodeheaderId();

    /**
     * Set id
     * @param string $productredemptioncodeheaderId
     * @return \Aureatelabs\RedemptionCode\ProductRedemptionCodeHeader\Api\Data\ProductRedemptionCodeHeaderInterface
     */
    public function setProductredemptioncodeheaderId($productredemptioncodeheaderId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Aureatelabs\RedemptionCode\ProductRedemptionCodeHeader\Api\Data\ProductRedemptionCodeHeaderInterface
     */
    public function setId($id);

    /**
     * Get product_id
     * @return string|null
     */
    public function getProductId();

    /**
     * Set product_id
     * @param string $productId
     * @return \Aureatelabs\RedemptionCode\ProductRedemptionCodeHeader\Api\Data\ProductRedemptionCodeHeaderInterface
     */
    public function setProductId($productId);

    /**
     * Get redemption_code_header_id
     * @return string|null
     */
    public function getRedemptionCodeHeaderId();

    /**
     * Set redemption_code_header_id
     * @param string $redemptionCodeHeaderId
     * @return \Aureatelabs\RedemptionCode\ProductRedemptionCodeHeader\Api\Data\ProductRedemptionCodeHeaderInterface
     */
    public function setRedemptionCodeHeaderId($redemptionCodeHeaderId);

    /**
     * Get created_timestamp
     * @return string|null
     */
    public function getCreatedTimestamp();

    /**
     * Set created_timestamp
     * @param string $createdTimestamp
     * @return \Aureatelabs\RedemptionCode\ProductRedemptionCodeHeader\Api\Data\ProductRedemptionCodeHeaderInterface
     */
    public function setCreatedTimestamp($createdTimestamp);

    /**
     * Get updated_timestamp
     * @return string|null
     */
    public function getUpdatedTimestamp();

    /**
     * Set updated_timestamp
     * @param string $updatedTimestamp
     * @return \Aureatelabs\RedemptionCode\ProductRedemptionCodeHeader\Api\Data\ProductRedemptionCodeHeaderInterface
     */
    public function setUpdatedTimestamp($updatedTimestamp);
}
