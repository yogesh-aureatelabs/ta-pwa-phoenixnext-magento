<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api\Data;

interface RedemptionCodeHeaderSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get RedemptionCodeHeader list.
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
