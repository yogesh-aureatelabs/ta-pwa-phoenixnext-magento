<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api\Data;

interface RedemptionCodeHeaderInterface
{

    const NAME = 'name';
    const QTY_IN_STOCK = 'qty_in_stock';
    const QTY_AVAILABLE = 'qty_available';
    const UPDATED_TIMESTAMP = 'updated_timestamp';
    const ID = 'id';
    const CREATED_TIMESTAMP = 'created_timestamp';
    const REDEMPTIONCODEHEADER_ID = 'id';

    /**
     * Get redemptioncodeheader_id
     * @return string|null
     */
    public function getRedemptioncodeheaderId();

    /**
     * Set redemptioncodeheader_id
     * @param string $redemptioncodeheaderId
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setRedemptioncodeheaderId($redemptioncodeheaderId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setId($id);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setName($name);

    /**
     * Get qty_in_stock
     * @return string|null
     */
    public function getQtyInStock();

    /**
     * Set qty_in_stock
     * @param string $name
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setQtyInStock($qtyinstock);

    /**
     * Get qty_available
     * @return string|null
     */
    public function getQtyAvailable();

    /**
     * Set qty_available
     * @param string $name
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setQtyAvailable($qtyavailable);

    /**
     * Get created_timestamp
     * @return string|null
     */
    public function getCreatedTimestamp();

    /**
     * Set created_timestamp
     * @param string $createdTimestamp
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setCreatedTimestamp($createdTimestamp);

    /**
     * Get updated_timestamp
     * @return string|null
     */
    public function getUpdatedTimestamp();

    /**
     * Set updated_timestamp
     * @param string $updatedTimestamp
     * @return \Aureatelabs\RedemptionCode\RedemptionCodeHeader\Api\Data\RedemptionCodeHeaderInterface
     */
    public function setUpdatedTimestamp($updatedTimestamp);
}
