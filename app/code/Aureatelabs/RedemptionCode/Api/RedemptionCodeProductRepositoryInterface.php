<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RedemptionCodeProductRepositoryInterface
{

    /**
     * Save RedemptionCodeProduct
     * @param \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface $redemptionCodeProduct
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface $redemptionCodeProduct
    );

    /**
     * Retrieve RedemptionCodeProduct
     * @param string $redemptioncodeproductId
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($redemptioncodeproductId);

    /**
     * Retrieve RedemptionCodeProduct matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete RedemptionCodeProduct
     * @param \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface $redemptionCodeProduct
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface $redemptionCodeProduct
    );

    /**
     * Delete RedemptionCodeProduct by ID
     * @param string $redemptioncodeproductId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($redemptioncodeproductId);
}

