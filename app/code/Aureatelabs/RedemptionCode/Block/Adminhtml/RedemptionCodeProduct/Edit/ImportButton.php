<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Block\Adminhtml\RedemptionCodeProduct\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class ImportButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Import Data'),
            'class' => 'secondary',
            'id' => 'click-me',
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'sort_order' => 10
        ];
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('adminhtml/system_config/edit/section/redemptioncodeimport');
    }
}
