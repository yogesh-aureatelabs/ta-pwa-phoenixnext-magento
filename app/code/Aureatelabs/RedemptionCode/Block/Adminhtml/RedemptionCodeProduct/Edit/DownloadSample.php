<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Block\Adminhtml\RedemptionCodeProduct\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class DownloadSample extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Download Sample Import'),
            'class' => 'secondary',
            'id' => 'download-sample',
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'sort_order' => 20
        ];
    }
    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('aureatelabs_redemptioncode/redemptioncodeproduct/downloadSample');
    }
}
