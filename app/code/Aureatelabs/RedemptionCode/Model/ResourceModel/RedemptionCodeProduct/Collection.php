<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Aureatelabs\RedemptionCode\Model\RedemptionCodeProduct::class,
            \Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct::class
        );
    }
}
