<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class RedemptionCodeHeader extends AbstractDb
{

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('redemption_code_header', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        exit('43453453');
    }

    public function getData()
    {
        echo $this->request->getParam('current_product_id');
    }
}
