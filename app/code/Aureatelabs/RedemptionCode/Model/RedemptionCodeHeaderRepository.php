<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model;

use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface;
use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterfaceFactory;
use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderSearchResultsInterfaceFactory;
use Aureatelabs\RedemptionCode\Api\RedemptionCodeHeaderRepositoryInterface;
use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeHeader as ResourceRedemptionCodeHeader;
use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeHeader\CollectionFactory as RedemptionCodeHeaderCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class RedemptionCodeHeaderRepository implements RedemptionCodeHeaderRepositoryInterface
{

    /**
     * @var RedemptionCodeHeaderCollectionFactory
     */
    protected $redemptionCodeHeaderCollectionFactory;

    /**
     * @var RedemptionCodeHeader
     */
    protected $searchResultsFactory;

    /**
     * @var RedemptionCodeHeaderInterfaceFactory
     */
    protected $redemptionCodeHeaderFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ResourceRedemptionCodeHeader
     */
    protected $resource;


    /**
     * @param ResourceRedemptionCodeHeader $resource
     * @param RedemptionCodeHeaderInterfaceFactory $redemptionCodeHeaderFactory
     * @param RedemptionCodeHeaderCollectionFactory $redemptionCodeHeaderCollectionFactory
     * @param RedemptionCodeHeaderSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceRedemptionCodeHeader $resource,
        RedemptionCodeHeaderInterfaceFactory $redemptionCodeHeaderFactory,
        RedemptionCodeHeaderCollectionFactory $redemptionCodeHeaderCollectionFactory,
        RedemptionCodeHeaderSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->redemptionCodeHeaderFactory = $redemptionCodeHeaderFactory;
        $this->redemptionCodeHeaderCollectionFactory = $redemptionCodeHeaderCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        RedemptionCodeHeaderInterface $redemptionCodeHeader
    ) {
        try {
            $this->resource->save($redemptionCodeHeader);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the redemptionCodeHeader: %1',
                $exception->getMessage()
            ));
        }
        return $redemptionCodeHeader;
    }

    /**
     * @inheritDoc
     */
    public function get($redemptionCodeHeaderId)
    {
        $redemptionCodeHeader = $this->redemptionCodeHeaderFactory->create();
        $this->resource->load($redemptionCodeHeader, $redemptionCodeHeaderId);
        if (!$redemptionCodeHeader->getId()) {
            throw new NoSuchEntityException(__('RedemptionCodeHeader with id "%1" does not exist.', $redemptionCodeHeaderId));
        }
        return $redemptionCodeHeader;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->redemptionCodeHeaderCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        RedemptionCodeHeaderInterface $redemptionCodeHeader
    ) {
        try {
            $redemptionCodeHeaderModel = $this->redemptionCodeHeaderFactory->create();
            $this->resource->load($redemptionCodeHeaderModel, $redemptionCodeHeader->getRedemptioncodeheaderId());
            $this->resource->delete($redemptionCodeHeaderModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the RedemptionCodeHeader: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($redemptionCodeHeaderId)
    {
        return $this->delete($this->get($redemptionCodeHeaderId));
    }
}
