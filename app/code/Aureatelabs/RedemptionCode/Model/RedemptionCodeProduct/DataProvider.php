<?php

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model\RedemptionCodeProduct;

use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @inheritDoc
     */
    protected $collection;


    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        \Aureatelabs\RedemptionCode\Helper\Data $helper,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->helper = $helper;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @inheritDoc
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $rec = $model->getData();
            $rec['edit'] = true;
            
            $rec["redemption_code"] = $this->helper->stringToSecret($rec['redemption_code']);
            
            $this->loadedData[$model->getId()] = $rec;
        }
        $data = $this->dataPersistor->get('redemption_code_item');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()]   = $model->getData();
            $this->dataPersistor->clear('redemption_code_item');
        }

        return $this->loadedData;
    }
}
