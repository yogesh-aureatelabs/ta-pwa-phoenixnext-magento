<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model;

use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface;
use Magento\Framework\Model\AbstractModel;

class RedemptionCodeProduct extends AbstractModel implements RedemptionCodeProductInterface
{
    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'redemption_code_product';

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct::class);
    }

    /**
     * @inheritDoc
     */
    public function getRedemptioncodeproductId()
    {
        return $this->getData(self::REDEMPTIONCODEPRODUCT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setRedemptioncodeproductId($redemptioncodeproductId)
    {
        return $this->setData(self::REDEMPTIONCODEPRODUCT_ID, $redemptioncodeproductId);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getRedemptionCode()
    {
        return $this->getData(self::REDEMPTION_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setRedemptionCode($redemptionCode)
    {
        return $this->setData(self::REDEMPTION_CODE, $redemptionCode);
    }

    /**
     * @inheritDoc
     */
    public function getRedemptionCodeHeaderId()
    {
        return $this->getData(self::REDEMPTION_CODE_HEADER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setRedemptionCodeHeaderId($redemptionCodeHeaderId)
    {
        return $this->setData(self::REDEMPTION_CODE_HEADER_ID, $redemptionCodeHeaderId);
    }

    /**
     * @inheritDoc
     */
    public function getRedemptionCodeItemNumber()
    {
        return $this->getData(self::REDEMPTION_CODE_ITEM_NUMBER);
    }

    /**
     * @inheritDoc
     */
    public function setRedemptionCodeItemNumber($redemptionCodeItemNumber)
    {
        return $this->setData(self::REDEMPTION_CODE_ITEM_NUMBER, $redemptionCodeItemNumber);
    }

    /**
     * @inheritDoc
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedTimestamp()
    {
        return $this->getData(self::CREATED_TIMESTAMP);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedTimestamp($createdTimestamp)
    {
        return $this->setData(self::CREATED_TIMESTAMP, $createdTimestamp);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedTimestamp()
    {
        return $this->getData(self::UPDATED_TIMESTAMP);
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedTimestamp($updatedTimestamp)
    {
        return $this->getData(self::UPDATED_TIMESTAMP, $updatedTimestamp);
    }

    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->setData(self::SKU);
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * @return RedemptionCodeProduct|mixed
     */
    public function getEmail()
    {
        return $this->setData(self::EMAIL);
    }

    /**
     * @param $email
     * @return RedemptionCodeProduct|mixed
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * @inheritDoc
     */
    public function getIsPurchased()
    {
        return $this->getData(self::IS_PURCHASED);
    }

    /**
     * @inheritDoc
     */
    public function setIsPurchased($isPurchased)
    {
        return $this->setData(self::IS_PURCHASED, $isPurchased);
    }
}
