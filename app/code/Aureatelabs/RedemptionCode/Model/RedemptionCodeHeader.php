<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model;

use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface;
use Magento\Framework\Model\AbstractModel;

class RedemptionCodeHeader extends AbstractModel implements RedemptionCodeHeaderInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeHeader::class);
    }

    /**
     * @inheritDoc
     */
    public function getRedemptioncodeheaderId()
    {
        return $this->getData(self::REDEMPTIONCODEHEADER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setRedemptioncodeheaderId($redemptioncodeheaderId)
    {
        return $this->setData(self::REDEMPTIONCODEHEADER_ID, $redemptioncodeheaderId);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getQtyInStock()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setQtyInStock($qtyinstock)
    {
        return $this->setData(self::QTY_IN_STOCK, $qtyinstock);
    }

    /**
     * @inheritDoc
     */
    public function getQtyAvailable()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setQtyAvailable($qtyavailable)
    {
        return $this->setData(self::QTY_AVAILABLE, $qtyavailable);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedTimestamp()
    {
        return $this->getData(self::CREATED_TIMESTAMP);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedTimestamp($createdTimestamp)
    {
        return $this->setData(self::CREATED_TIMESTAMP, $createdTimestamp);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedTimestamp()
    {
        return $this->getData(self::UPDATED_TIMESTAMP);
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedTimestamp($updatedTimestamp)
    {
        return $this->setData(self::UPDATED_TIMESTAMP, $updatedTimestamp);
    }
}
