<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model;

use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterface;
use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductInterfaceFactory;
use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeProductSearchResultsInterfaceFactory;
use Aureatelabs\RedemptionCode\Api\RedemptionCodeProductRepositoryInterface;
use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct as ResourceRedemptionCodeProduct;
use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct\CollectionFactory as RedemptionCodeProductCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class RedemptionCodeProductRepository implements RedemptionCodeProductRepositoryInterface
{

    /**
     * @var RedemptionCodeProductInterfaceFactory
     */
    protected $redemptionCodeProductFactory;

    /**
     * @var RedemptionCodeProduct
     */
    protected $searchResultsFactory;

    /**
     * @var RedemptionCodeProductCollectionFactory
     */
    protected $redemptionCodeProductCollectionFactory;

    /**
     * @var ResourceRedemptionCodeProduct
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;


    /**
     * @param ResourceRedemptionCodeProduct $resource
     * @param RedemptionCodeProductInterfaceFactory $redemptionCodeProductFactory
     * @param RedemptionCodeProductCollectionFactory $redemptionCodeProductCollectionFactory
     * @param RedemptionCodeProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceRedemptionCodeProduct $resource,
        RedemptionCodeProductInterfaceFactory $redemptionCodeProductFactory,
        RedemptionCodeProductCollectionFactory $redemptionCodeProductCollectionFactory,
        RedemptionCodeProductSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->redemptionCodeProductFactory = $redemptionCodeProductFactory;
        $this->redemptionCodeProductCollectionFactory = $redemptionCodeProductCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        RedemptionCodeProductInterface $redemptionCodeProduct
    ) {
        try {
            $this->resource->save($redemptionCodeProduct);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the redemptionCodeProduct: %1',
                $exception->getMessage()
            ));
        }
        return $redemptionCodeProduct;
    }

    /**
     * @inheritDoc
     */
    public function get($redemptionCodeProductId)
    {
        $redemptionCodeProduct = $this->redemptionCodeProductFactory->create();
        $this->resource->load($redemptionCodeProduct, $redemptionCodeProductId);
        if (!$redemptionCodeProduct->getId()) {
            throw new NoSuchEntityException(__('RedemptionCodeProduct with id "%1" does not exist.', $redemptionCodeProductId));
        }
        return $redemptionCodeProduct;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->redemptionCodeProductCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        RedemptionCodeProductInterface $redemptionCodeProduct
    ) {
        try {
            $redemptionCodeProductModel = $this->redemptionCodeProductFactory->create();
            $this->resource->load($redemptionCodeProductModel, $redemptionCodeProduct->getRedemptioncodeproductId());
            $this->resource->delete($redemptionCodeProductModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the RedemptionCodeProduct: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($redemptionCodeProductId)
    {
        return $this->delete($this->get($redemptionCodeProductId));
    }
}

