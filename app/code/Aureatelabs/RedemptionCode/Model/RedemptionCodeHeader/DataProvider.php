<?php

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Model\RedemptionCodeHeader;

use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeHeader\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
       $name,
       $primaryFieldName,
       $requestFieldName,
       CollectionFactory $collectionFactory,
       DataPersistorInterface $dataPersistor,
       array $meta = [],
       array $data = []
    ) {
       $this->collection = $collectionFactory->create();
       $this->dataPersistor = $dataPersistor;
       parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
        }
        $data = $this->dataPersistor->get('redemption_code_header');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('redemption_code_header');
        }

        return $this->loadedData;
    }
}
