<?php

namespace Aureatelabs\RedemptionCode\Model\Config\Backend;

class CustomFileType extends \Magento\Config\Model\Config\Backend\File
{
    public function _getAllowedExtensions()
    {
        return ['csv', 'xls'];
    }

    protected function _getDeleteCheckbox()
    {
        return '';
    }
}
