<?php

namespace Aureatelabs\RedemptionCode\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        /*
        * Create table 'redemption_code_header'
        */
        $redemptionCodeHeader = $installer->getConnection()->newTable(
            $installer->getTable('redemption_code_header')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Header Id'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Header Name'
        )->addColumn(
            'qty_in_stock',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Count all redemption_code_item related to the header with is_bought = 0'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            ['nullable' => false]
        )->addColumn(
            'created_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->setComment(
            'PN Data Dictionary Header'
        );

        /*
        * Create table 'redemption_code_item'
        */
        $redemptionCodeItem = $installer->getConnection()->newTable(
            $installer->getTable('redemption_code_item')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Record Id'
        )->addColumn(
            'redemption_code_item_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Running number for each Redemption Code Header communicate since the redemption_code is displayed with masking'
        )->addColumn(
            'redemption_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => false],
            'Pattern is 00000-00000-00000 (as of 2022 Dec)'
        )->addColumn(
            'redemption_code_header_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'index' => true],
            'Link to redemption_code_header table'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['index' => true, 'nullable' => true, 'default' => null],
            'quote.reserve_order_id or sales_order.increment_id that purchase the code. This is for customer support'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true, 'default' => null],
            'Customer\'s email that the code is sent to'
        )->addColumn(
            'sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true, 'default' => null],
            'True means reserved by Cart (quote_item) and False means not reserved'
        )->addColumn(
            'is_purchased',
            \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
            null,
            ['default' => 0],
            'True means the code is bought and False means the code is available'
        )->addColumn(
            'created_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'email_sent_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => true, 'default' => null],
            'Customer\'s email that the code is sent to'
        )->addColumn(
            'updated_timestamp',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addForeignKey(
            $installer->getFkName(
                'redemption_code_item',
                'redemption_code_header_id',
                'redemption_code_header',
                'id'
            ),
            'redemption_code_header_id',
            $installer->getTable('redemption_code_header'),
            'id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'PN Data Dictionary'
        );

        $installer->getConnection()->createTable($redemptionCodeHeader);
        $installer->getConnection()->createTable($redemptionCodeItem);
        $installer->endSetup();
    }
}
