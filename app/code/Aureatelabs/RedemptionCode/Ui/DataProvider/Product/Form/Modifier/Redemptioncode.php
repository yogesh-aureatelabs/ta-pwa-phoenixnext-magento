<?php

namespace Aureatelabs\RedemptionCode\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Aureatelabs\RedemptionCode\Api\Data\RedemptionCodeHeaderInterface;
use Magento\Catalog\Api\Data\ProductLinkInterface;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Modal;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Catalog\Api\ProductLinkRepositoryInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Aureatelabs\RedemptionCode\Model\RedemptionCodeHeader;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;

class Redemptioncode extends AbstractModifier
{
    /**
     * @var string
     * @since 101.0.0
     */
    protected $scopePrefix;
    /**
     * @var string
     * @since 101.0.0
     */
    protected $scopeName;
    /**
     * @var UrlInterface
     * @since 101.0.0
     */
    protected $urlBuilder;
    /**
     * @var ProductLinkRepositoryInterface
     */
    protected $productLinkRepository;
    /**
     * @var LocatorInterface
     */
    protected $locator;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var AttributeSetRepositoryInterface
     */
    protected $attributeSetRepository;
    /**
     * @var \Magento\Catalog\Ui\Component\Listing\Columns\Price
     */
    private $priceModifier;

    /**
     * @var Product
     */
    private $redemptionCodeItem;

    const DATA_SCOPE = '';

    const DATA_SCOPE_COMPANION = 'redemptioncode';

    const GROUP_COMPANION = 'redemptioncode';

    /**
     * redemptioncode constructor.
     * 
     * @param UrlInterface $urlBuilder
     * @param Product $redemptionCodeItem
     * @param ProductLinkRepositoryInterface $productLinkRepository
     * @param LocatorInterface $locator
     * @param ProductRepositoryInterface $productRepository
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param string $scopePrefix
     * @param string $scopeName
     */
    public function __construct(
        UrlInterface $urlBuilder,
        RedemptionCodeHeader $redemptionCodeItem,
        ProductLinkRepositoryInterface $productLinkRepository,
        LocatorInterface $locator,
        ProductRepositoryInterface $productRepository,
        AttributeSetRepositoryInterface $attributeSetRepository,
        $scopePrefix = '',
        $scopeName = ''
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->locator = $locator;
        $this->redemptionCodeItem = $redemptionCodeItem;
        $this->scopePrefix = $scopePrefix;
        $this->productRepository = $productRepository;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->productLinkRepository = $productLinkRepository;
        $this->scopeName = $scopeName;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        $meta['companion'] = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Redemption Code Attributes'),
                        'sortOrder' => 60,
                        'collapsible' => true,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => static::DATA_SCOPE,
                    ]
                ]
            ],
            'children' => [
                $this->scopePrefix . static::DATA_SCOPE_COMPANION => $this->getCompanionFieldset(),
            ]
        ];

        return $meta;
    }

    /**
     * @param array $data
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function modifyData(array $data)
    {
        /** @var \Aureatelabs\RedemptionCode\Model\RedemptionCodeHeader $header */
        $headerId = $header->getId();

        if (!$headerId) {
            return $data;
        }

        $data[$headerId][self::DATA_SOURCE_DEFAULT]['current_header_id'] = $headerId;
        $data[$headerId][self::DATA_SOURCE_DEFAULT]['current_store_id'] = $this->locator->getStore()->getId();

        return $data;
    }

    /**
     * @param RedemptionCodeHeaderInterface $linkedProduct
     * @param ProductLinkInterface $linkItem
     * @param $productId
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function fillData(RedemptionCodeHeaderInterface $linkedProduct, ProductLinkInterface $linkItem, $productId)
    {
        return [
            'id' => $linkedProduct->getId(),
            'name' => $linkedProduct->getName(),
            'qty_in_stock' => $linkedProduct->getQtyInStock(),
            'qty_available' => $linkedProduct->getQtyAvailable(),
        ];
    }

    /**
     * Retrieve all data scopes
     *
     * @return array
     * @since 101.0.0
     */
    protected function getDataScopes()
    {
        return [
            static::DATA_SCOPE_COMPANION
        ];
    }

    /**
     * Prepares config for the Companion products fieldset
     *
     * @return array
     * @since 101.0.0
     */
    protected function getCompanionFieldset()
    {
        $content = __(
            'Redemption code headers are shown to customers in addition to the item the customer is looking at.'
        );

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Redemption Code Header'),
                    $this->scopePrefix . static::DATA_SCOPE_COMPANION
                ),
                'modal' => $this->getGenericModal(
                    __('Add Redemption Code Header'),
                    $this->scopePrefix . static::DATA_SCOPE_COMPANION
                ),
                static::DATA_SCOPE_COMPANION => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_COMPANION),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => __('Redemption Code Header Attributessss'),
                        'collapsible' => false,
                        'componentType' => Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 10,
                    ],
                ],
            ]
        ];
    }

    /**
     * Retrieve button set
     *
     * @param Phrase $content
     * @param Phrase $buttonTitle
     * @param string $scope
     * @return array
     * @since 101.0.0
     */
    protected function getButtonSet(Phrase $content, Phrase $buttonTitle, $scope)
    {
        $modalTarget = $this->scopeName . '.' . static::GROUP_COMPANION . '.' . $scope . '.modal';

        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'label' => false,
                        'content' => $content,
                        'template' => 'ui/form/components/complex',
                    ],
                ],
            ],
            'children' => [
                'button_' . $scope => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'formElement' => 'container',
                                'componentType' => 'container',
                                'component' => 'Magento_Ui/js/form/components/button',
                                'actions' => [
                                    [
                                        'targetName' => $modalTarget,
                                        'actionName' => 'toggleModal',
                                    ],
                                    [
                                        'targetName' => $modalTarget . '.' . $scope . '_product_listing',
                                        'actionName' => 'render',
                                    ]
                                ],
                                'title' => $buttonTitle,
                                'provider' => null,
                            ],
                        ],
                    ],

                ],
            ],
        ];
    }

    /**
     * Prepares config for modal slide-out panel
     *
     * @param Phrase $title
     * @param string $scope
     * @return array
     * @since 101.0.0
     */
    protected function getGenericModal(Phrase $title, $scope)
    {
        $listingTarget = 'redemption_code_header_listing';

        $modal = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Modal::NAME,
                        'dataScope' => '',
                        'options' => [
                            'title' => $title,
                            'buttons' => [
                                [
                                    'text' => __('Cancel'),
                                    'actions' => [
                                        'closeModal'
                                    ]
                                ],
                                [
                                    'text' => __('Add Redemption Code Header'),
                                    'class' => 'action-primary',
                                    'actions' => [
                                        [
                                            'targetName' => 'index = ' . $listingTarget,
                                            'actionName' => 'save'
                                        ],
                                        'closeModal'
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'children' => [
                $listingTarget => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'autoRender' => false,
                                'componentType' => 'insertListing',
                                'dataScope' => $listingTarget,
                                'externalProvider' => 'redemption_code_header_listing.product_related_redemptioncodeheader_listing_data_source',
                                'selectionsProvider' => $listingTarget . '.' . $listingTarget . '.customsize_records_columns.ids',
                                'ns' => $listingTarget,
                                'render_url' => $this->urlBuilder->getUrl('mui/index/render'),
                                'realTimeLink' => true,
                                'dataLinks' => [
                                    'imports' => false,
                                    'exports' => true
                                ],
                                'behaviourType' => 'simple',
                                'externalFilterMode' => true,
                                'imports' => [
                                    'productId' => '${ $.provider }:data.header.current_product_id',
                                    'storeId' => '${ $.provider }:data.header.current_store_id',
                                    '__disableTmpl' => ['productId' => false, 'storeId' => false],
                                ],
                                'exports' => [
                                    'productId' => '${ $.externalProvider }:params.current_header_id',
                                    'storeId' => '${ $.externalProvider }:params.current_store_id',
                                    '__disableTmpl' => ['productId' => false, 'storeId' => false],
                                ]
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return $modal;
    }

    /**
     * Retrieve grid
     *
     * @param string $scope
     * @return array
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @since 101.0.0
     */
    protected function getGrid($scope)
    {
        $dataProvider = 'redemption_code_header_listing';

        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__field-wide',
                        'componentType' => DynamicRows::NAME,
                        'label' => null,
                        'columnsHeader' => false,
                        'columnsHeaderAfterRender' => true,
                        'renderDefaultRecord' => false,
                        'template' => 'ui/dynamic-rows/templates/grid',
                        'component' => 'Magento_Ui/js/dynamic-rows/dynamic-rows-grid',
                        'addButton' => false,
                        'recordTemplate' => 'record',
                        'dataScope' => 'data.links',
                        'deleteButtonLabel' => __('Remove'),
                        'dataProvider' => $dataProvider,
                        'map' => [
                            'id' => 'id',
                            'name' => 'name',
                        ],
                        'links' => [
                            'insertData' => '${ $.provider }:${ $.dataProvider }',
                            '__disableTmpl' => ['insertData' => false],
                        ],
                        'sortOrder' => 2,
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => 'container',
                                'isTemplate' => true,
                                'is_collection' => true,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope' => '',
                            ],
                        ],
                    ],
                    'children' => $this->fillMeta(),
                ],
            ],
        ];
    }

    /**
     * Retrieve meta column
     *
     * @return array
     * @since 101.0.0
     */
    protected function fillMeta()
    {
        return [
            'id' => $this->getTextColumn('id', false, __('ID'), 0),
            'name' => $this->getTextColumn('name', false, __('Name'), 20),
            'qty_in_stock' => $this->getTextColumn('qty_in_stock', true, __('Status'), 30),
            'qty_available' => $this->getTextColumn('qty_available', false, __('Attribute Set'), 40),
            'actionDelete' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'additionalClasses' => 'data-grid-actions-cell',
                            'componentType' => 'actionDelete',
                            'dataType' => Text::NAME,
                            'label' => __('Actions'),
                            'sortOrder' => 80,
                            'fit' => true,
                        ],
                    ],
                ],
            ],
            'position' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => Number::NAME,
                            'formElement' => Input::NAME,
                            'componentType' => Field::NAME,
                            'dataScope' => 'position',
                            'sortOrder' => 90,
                            'visible' => false,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Retrieve text column structure
     *
     * @param string $dataScope
     * @param bool $fit
     * @param Phrase $label
     * @param int $sortOrder
     * @return array
     * @since 101.0.0
     */
    protected function getTextColumn($dataScope, $fit, Phrase $label, $sortOrder)
    {
        $column = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'elementTmpl' => 'ui/dynamic-rows/cells/text',
                        'component' => 'Magento_Ui/js/form/element/text',
                        'dataType' => Text::NAME,
                        'dataScope' => $dataScope,
                        'fit' => $fit,
                        'label' => $label,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];

        return $column;
    }

    // public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    // {
    //     return null;
    // }

    // public function validateForCsrf(RequestInterface $request): ?bool
    // {
    //     return true;
    // }
}
