<?php

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Ui\Component\Listing\Column;

class RedemptionCodeProductActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    const URL_PATH_EDIT = 'aureatelabs_redemptioncode/redemptioncodeproduct/edit';
    const URL_PATH_DETAILS = 'aureatelabs_redemptioncode/redemptioncodeproduct/details';
    const URL_PATH_DELETE = 'aureatelabs_redemptioncode/redemptioncodeproduct/delete';
    const URL_PATH_RESEND = 'aureatelabs_redemptioncode/redemptioncodeproduct/resend';

    protected $urlBuilder;
    protected $redemptionHeader;
    protected $salesOrder;
    protected $resource;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Aureatelabs\RedemptionCode\Model\RedemptionCodeHeaderRepository $headerRepository
     * @param \Magento\Sales\Api\OrderItemRepositoryInterface $salesOrderRepository
     * @param \Magento\Framework\App\ResourceConnection $resourceConnectiony
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Aureatelabs\RedemptionCode\Helper\Data $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Aureatelabs\RedemptionCode\Model\RedemptionCodeHeaderRepository $headerRepository,
        \Magento\Sales\Api\OrderItemRepositoryInterface $salesOrderRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Aureatelabs\RedemptionCode\Helper\Data $helper,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->redemptionHeader = $headerRepository;
        $this->salesOrder = $salesOrderRepository;
        $this->resource = $resourceConnection;
        $this->helper = $helper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['id'])) {
                    $purchased = $item['is_purchased'];
                    $item['is_purchased'] = $purchased == 1 ? __('Yes') : __('No');
                    $item['redemption_code'] = $this->helper->stringToSecret($item['redemption_code']);
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Edit')
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'id' => $item['id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete a code:"' . $item['redemption_code'] . '" '),
                                'message' => __('Are you sure you wan\'t to delete a code:"' . $item['redemption_code'] . '" record?')
                            ]
                        ]
                    ];

                    if($item['email']){
                        $item[$this->getData('name')]['resend'] =  
                            [
                                'href' => $this->urlBuilder->getUrl(
                                    static::URL_PATH_RESEND,
                                    [
                                        'id' => $item['id']
                                    ]
                                ),
                                'label' => __('Resend Email'),
                                'confirm' => [
                                    'title' => __('Resend Email to :"' . $item['email'] . '" '),
                                    'message' => __('Are you sure you want to resend redemption code to:"' . $item['email'] . '"?')
                                ]
                            ];    
                    }
                }
            }
        }

        return $dataSource;
    }
    
}
