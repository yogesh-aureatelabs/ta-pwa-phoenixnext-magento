<?php

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Ui\Component\Listing\Column\Header;

class ViewAction extends \Magento\Ui\Component\Listing\Columns\Column
{

    const URL_PATH_CODE_GRID = 'aureatelabs_redemptioncode/redemptioncodeproduct/index';

    protected $urlBuilder;
    

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                    $item[$this->getData('name')] = [
                        'view' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_CODE_GRID,
                                [
                                    'redemption_code_header_id' => $item['id']
                                ]
                            ),
                            'label' => __('View Codes')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}

