<?php

namespace Aureatelabs\RedemptionCode\Ui\Component\Form\SalesOrderItem;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Options
 */
class Options implements OptionSourceInterface
{
    const SALES_ORDER_ITEM_TABLE = 'sales_order_item';
    public $resourceConnection;

    /**
     * __construct
     *
     * @param  ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Return array of options as value-label pairs
     */
    public function toOptionArray(): array
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName(self::SALES_ORDER_ITEM_TABLE);

        $query = $connection->select()
            ->from($tableName, ['item_id', 'name']);

        $salesOrders = $connection->fetchAll($query);

        $arrResult = [];
        foreach ($salesOrders as $item) {
            $arrResult[] = ['value' => $item['item_id'], 'label' => $item['name']];
        }

        return $arrResult;
    }
}
