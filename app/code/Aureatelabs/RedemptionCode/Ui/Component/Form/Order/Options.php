<?php

namespace Aureatelabs\RedemptionCode\Ui\Component\Form\Order;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Options
 */
class Options implements OptionSourceInterface
{
    const ORDER_TABLE = 'sales_order';
    public $resourceConnection;

    /**
     * __construct
     *
     * @param  ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Return array of options as value-label pairs
     */
    public function toOptionArray(): array
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName(self::ORDER_TABLE);

        $query = $connection->select()
            ->from($tableName, ['increment_id']);

        $orders = $connection->fetchAll($query);

        $arrResult = [];
        foreach ($orders as $order) {
            $arrResult[] = ['value' => $order['increment_id'], 'label' => $order['increment_id']];
        }

        return $arrResult;
    }
}
