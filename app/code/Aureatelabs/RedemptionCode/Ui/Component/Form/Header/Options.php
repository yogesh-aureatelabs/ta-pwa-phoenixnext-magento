<?php

namespace Aureatelabs\RedemptionCode\Ui\Component\Form\Header;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Options
 */
class Options implements OptionSourceInterface
{
    const REDEMPTION_CODE_HEADER_TABLE = 'redemption_code_header';
    public $resourceConnection;

    /**
     * __construct
     *
     * @param  ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Return array of options as value-label pairs
     */
    public function toOptionArray(): array
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName(self::REDEMPTION_CODE_HEADER_TABLE);

        $query = $connection->select()
            ->from($tableName, ['id', 'name']);

        $redemptionHeaders = $connection->fetchAll($query);

        $arrResult = [];
        foreach ($redemptionHeaders as $header) {
            $arrResult[] = ['value' => $header['id'], 'label' => $header['name']];
        }

        return $arrResult;
    }
}
