<?php

namespace Aureatelabs\RedemptionCode\Ui\Component\Form\QuoteItem;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Options
 */
class Options implements OptionSourceInterface
{
    const QUOTE_ITEM_TABLE = 'quote_item';
    public $resourceConnection;

    /**
     * __construct
     *
     * @param  ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Return array of options as value-label pairs
     */
    public function toOptionArray(): array
    {
        $connection  = $this->resourceConnection->getConnection();
        $tableName = $connection->getTableName(self::QUOTE_ITEM_TABLE);

        $query = $connection->select()
            ->from($tableName, ['item_id', 'name']);

        $quoteitems = $connection->fetchAll($query);

        $arrResult = [];
        foreach ($quoteitems as $item) {
            $arrResult[] = ['value' => $item['item_id'], 'label' => $item['name']];
        }

        return $arrResult;
    }
}
