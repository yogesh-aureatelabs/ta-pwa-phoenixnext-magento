<?php
namespace Aureatelabs\RedemptionCode\Helper;

class Data
{
    public function stringToSecret(string $string = NULL)
    {
        if (!$string) {
            return NULL;
        }
        $length = strlen($string);
        $visibleCount = (int) round($length / 4);
        $hiddenCount = $length - ($visibleCount * 2);
        return substr($string, 0, $visibleCount) . str_repeat('X', $hiddenCount) . substr($string, ($visibleCount * -1), $visibleCount);
    }
}