<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Controller\Adminhtml\RedemptionCodeHeader;

class Edit extends \Aureatelabs\RedemptionCode\Controller\Adminhtml\RedemptionCodeHeader
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create(\Aureatelabs\RedemptionCode\Model\RedemptionCodeHeader::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Redemption Code Header no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('aureatelabs_redemptioncode_redemptioncodeheader', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Redemption Code Header') : __('New Redemption Code Header'),
            $id ? __('Edit Redemption Code Header') : __('New Redemption Code Header')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Redemption code headers'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Redemption Code Header %1', $model->getId()) : __('New Redemption Code Header'));
        return $resultPage;
    }
}

