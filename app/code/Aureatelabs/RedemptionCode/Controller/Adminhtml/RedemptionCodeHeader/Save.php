<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Controller\Adminhtml\RedemptionCodeHeader;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $newRecord = 1;

            $model = $this->_objectManager->create(\Aureatelabs\RedemptionCode\Model\RedemptionCodeHeader::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Redemption Code Header no longer exists.'));
                return $resultRedirect->setPath('aureatelabs_redemptioncode/redemptioncodeheader/index');
            }

            if ($model->getId()) {
                $newRecord = 0;
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Redemption Code Header.'));
                $this->dataPersistor->clear('aureatelabs_redemptioncode_redemptioncodeheader');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('aureatelabs_redemptioncode/redemptioncodeheader/edit', ['id' => $model->getId()]);
                }

                if ($newRecord == 0) { // record is for edit
                    return $resultRedirect->setPath('aureatelabs_redemptioncode/redemptioncodeheader/index');
                } else {
                    return $resultRedirect->setPath('aureatelabs_redemptioncode/redemptioncodeheader/index');
                }
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Redemption Code Header.'));
            }

            $this->dataPersistor->set('aureatelabs_redemptioncode_redemptioncodeheader', $data);
            return $resultRedirect->setPath('aureatelabs_redemptioncode/redemptioncodeheader/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('aureatelabs_redemptioncode/redemptioncodeheader/index');
    }
}
