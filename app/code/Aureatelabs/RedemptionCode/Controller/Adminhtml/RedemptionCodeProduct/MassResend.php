<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Controller\Adminhtml\RedemptionCodeProduct;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Aureatelabs\RedemptionCode\Model\ResourceModel\RedemptionCodeProduct\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Aureatelabs\RedemptionCode\Api\RedemptionCodeProductRepositoryInterface;

class MassResend extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Filter $filter,
        CollectionFactory $collectionFactory,
        RedemptionCodeProductRepositoryInterface $redemptionCodeProductRepositoryInterface
    ) {
        $this->dataPersistor = $dataPersistor;
        
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->orderFactory = $orderFactory;
        $this->filter = $filter;
        $this->redemptionCodeProductRepositoryInterface = $redemptionCodeProductRepositoryInterface;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    /**
     * Mass Resend Redemption code action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $count = 0;
            $notFoundCount = 0;
            foreach ($collection as $model) {
                $modelData = $model->getData();
                if($modelData['order_id'] && $modelData['email'] && $modelData['redemption_code'] && $modelData['sku']){
                    $redemptionCode = $modelData['redemption_code'];
                    $orderId = $modelData['order_id'];
                    $email = $modelData['email'];
                    $order = $this->orderFactory->create()->loadByIncrementId($orderId);
                    $customerName = $order->getCustomerName();
                    $productName = $this->productFactory->create()->loadByAttribute('sku', $modelData['sku'])->getName();
                    $itemQty = 0;
                    foreach ($order->getAllVisibleItems() as $_item) {
                        if($_item == $modelData['sku']){
                            $itemQty = $_item->getQtyOrdered();
                            break;
                        }   
                    }

                    $this->_inlineTranslation->suspend();
                    $sender = [
                        'email' => $this->_scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE),
                        'name' =>  $this->_scopeConfig->getValue('trans_email/ident_general/name', ScopeInterface::SCOPE_STORE)
                    ];
                    
                    if ($redemptionCode && $redemptionCode != "") {
                        $itemsCoupons[] = $productName ." - ". $redemptionCode;
                        $to = $email;
                        $transport = $this->_transportBuilder
                            ->setTemplateIdentifier('redemption_coupons')
                            ->setTemplateOptions(
                                [
                                    'area' => 'frontend',
                                    'store' => $this->storeManager->getStore()->getId()
                                ]
                            )
                            ->setTemplateVars([
                                'coupons' => $itemsCoupons,
                                'order_id' => $orderId,
                                'product_name' => $productName,
                                'product_qty' => $itemQty,
                                'customer_name' => $customerName
                            ])
                            ->setFromByScope($sender)
                            ->addTo($to)
                            ->getTransport();

                        $transport->sendMessage();
                        $this->_inlineTranslation->resume();
                    }
                    $count++;
                }
                else{
                    $notFoundCount++;
                }
            }
            if($count){
                $this->messageManager->addSuccess(__('A total of %1 redemption code(s) sent.', $count));
            }
            if($notFoundCount){
                $this->messageManager->addError(__('A total of %1 redemption code(s) not sent due to the missing information.', $notFoundCount));
            }
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');        
    }
    // private function massResend(){

    // }

    // public function execute()
    // {
    //     /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
    //     $resultRedirect = $this->resultRedirectFactory->create();
    //     $id = $this->getRequest()->getParam('id');
    
    //     $model = $this->_objectManager->create(\Aureatelabs\RedemptionCode\Model\RedemptionCodeProduct::class)->load($id);
    //     if (!$model->getId() && $id) {
    //         $this->messageManager->addErrorMessage(__('This Redemption code no longer exists.'));
    //         return $resultRedirect->setPath('*/*/');
    //     }
    //     $modelData = $model->getData();
        
    //     $redemptionCode = $modelData['redemption_code'];
    //     $orderId = $modelData['order_id'];
        
        
    //     $email = $modelData['email'];
    //     $order = $this->orderFactory->create()->loadByIncrementId($orderId);
    //     $customerName = $order->getCustomerName();
    //     $productName = $this->productFactory->create()->loadByAttribute('sku', $modelData['sku'])->getName();
    //     $itemQty = 0;
    //     foreach ($order->getAllVisibleItems() as $_item) {
    //         if($_item == $modelData['sku']){
    //             $itemQty = $_item->getQtyOrdered();
    //             break;
    //         }   
    //     }
    //     try {
    //         $this->_inlineTranslation->suspend();
    //         $sender = [
    //             'email' => $this->_scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE),
    //             'name' =>  $this->_scopeConfig->getValue('trans_email/ident_general/name', ScopeInterface::SCOPE_STORE)
    //         ];
            
    //         if ($redemptionCode && $redemptionCode != "") {
    //             $itemsCoupons[] = $productName ." - ". $redemptionCode;
    //             $to = $email;
    //             $transport = $this->_transportBuilder
    //                 ->setTemplateIdentifier('redemption_coupons')
    //                 ->setTemplateOptions(
    //                     [
    //                         'area' => 'frontend',
    //                         'store' => $this->storeManager->getStore()->getId()
    //                     ]
    //                 )
    //                 ->setTemplateVars([
    //                     'coupons' => $itemsCoupons,
    //                     'order_id' => $orderId,
    //                     'product_name' => $productName,
    //                     'product_qty' => $itemQty,
    //                     'customer_name' => $customerName
    //                 ])
    //                 ->setFromByScope($sender)
    //                 ->addTo($to)
    //                 ->getTransport();

    //             $transport->sendMessage();
    //             $this->_inlineTranslation->resume();
    //             $this->messageManager->addSuccessMessage(__('Redemption code sent successfully.'));
    //         }
    //         else{
    //             $this->messageManager->addErrorMessage(__('No Redemption code found'));
    //         }
    //     } catch (\Exception $e) {
    //         $this->logLoggerInterface->debug($e->getMessage());
    //         $this->messageManager->addErrorMessage($e->getMessage());
    //     }
    //     return $resultRedirect->setPath('*/*/');
    // }
}

