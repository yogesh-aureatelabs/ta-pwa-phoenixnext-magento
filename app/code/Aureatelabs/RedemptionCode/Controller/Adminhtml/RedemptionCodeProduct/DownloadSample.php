<?php
namespace Aureatelabs\RedemptionCode\Controller\Adminhtml\RedemptionCodeProduct;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DirectoryList;
use Psr\Log\LoggerInterface;
 
class DownloadSample extends Action
{
    protected $downloader;
    protected $logger;
    protected $directory;
 
    public function __construct(Context $context,
                                FileFactory $fileFactory,
                                LoggerInterface $logger,
                                DirectoryList $directory)
    {
        $this->logger = $logger;
        $this->downloader = $fileFactory;
        $this->directory = $directory;
        parent::__construct($context);
    }
 
    public function execute()
    {
 
        $fileName = "sample-import-data.csv";
        $filePath = '';
 
        try {
            $filePath = $this->directory->getPath("media") . '/sample_redemptioncode/'.$fileName;
        } catch (FileSystemException $e) {
            $this->logger->info($e->getMessage());
        }
        try {
            return $this->downloader->create($fileName, [
                'type' => 'filename',
                'value' => $filePath,
            ],
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA,
                'application/octet-stream');
        } catch (\Exception $e) {
            $this->logger->info($e->getMessage());
        }
    }
}
