<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RedemptionCode\Controller\Adminhtml\RedemptionCodeProduct;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Resend extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        StateInterface $inlineTranslation,
        TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->dataPersistor = $dataPersistor;
        
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->productFactory = $productFactory;
        $this->orderFactory = $orderFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
    
        $model = $this->_objectManager->create(\Aureatelabs\RedemptionCode\Model\RedemptionCodeProduct::class)->load($id);
        if (!$model->getId() && $id) {
            $this->messageManager->addErrorMessage(__('This Redemption code no longer exists.'));
            return $resultRedirect->setPath('*/*/');
        }
        $modelData = $model->getData();
        
        $redemptionCode = $modelData['redemption_code'];
        $orderId = $modelData['order_id'];
        
        
        $email = $modelData['email'];
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);
        $customerName = $order->getCustomerName();
        $productName = $this->productFactory->create()->loadByAttribute('sku', $modelData['sku'])->getName();
        $itemQty = 0;
        foreach ($order->getAllVisibleItems() as $_item) {
            if($_item == $modelData['sku']){
                $itemQty = $_item->getQtyOrdered();
                break;
            }   
        }
        try {
            $this->_inlineTranslation->suspend();
            $sender = [
                'email' => $this->_scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE),
                'name' =>  $this->_scopeConfig->getValue('trans_email/ident_general/name', ScopeInterface::SCOPE_STORE)
            ];
            
            if ($redemptionCode && $redemptionCode != "") {
                $itemsCoupons[] = $productName ." - ". $redemptionCode;
                $to = $email;
                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier('redemption_coupons')
                    ->setTemplateOptions(
                        [
                            'area' => 'frontend',
                            'store' => $this->storeManager->getStore()->getId()
                        ]
                    )
                    ->setTemplateVars([
                        'coupons' => $itemsCoupons,
                        'order_id' => $orderId,
                        'product_name' => $productName,
                        'product_qty' => $itemQty,
                        'customer_name' => $customerName
                    ])
                    ->setFromByScope($sender)
                    ->addTo($to)
                    ->getTransport();

                $transport->sendMessage();
                $this->_inlineTranslation->resume();
                $this->messageManager->addSuccessMessage(__('Redemption code sent successfully.'));
            }
            else{
                $this->messageManager->addErrorMessage(__('No Redemption code found'));
            }
        } catch (\Exception $e) {
            $this->logLoggerInterface->debug($e->getMessage());
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $resultRedirect->setPath('*/*/');
    }
}

