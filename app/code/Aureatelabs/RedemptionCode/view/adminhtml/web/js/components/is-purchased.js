define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/single-checkbox',
    'Magento_Ui/js/modal/modal',
    'ko'
 ], function (_, uiRegistry, select, modal, ko) {
    'use strict';
    return select.extend({
        initialize: function () {
            this._super();
            this.fieldDepend(this.value());
            return this;
        },
        onUpdate: function (value)
        {
         console.log(value);
            var field_order_id = uiRegistry.get('index = order_id'); // get field
            var field_sku = uiRegistry.get('index = sku'); // get fieldset
            var field_email = uiRegistry.get('index = email'); // get fieldset
            if (value == 0) {
                field_order_id.hide();
                field_sku.hide();
                field_email.hide();
            }
            else {
                field_order_id.show();
                field_sku.show();
                field_email.show();
            }
            return this._super();
        },
        fieldDepend: function (value)
        {
            setTimeout( function(){
                var field_order_id = uiRegistry.get('index = order_id');
                var field_sku = uiRegistry.get('index = sku');
                var field_email = uiRegistry.get('index = email');
                if (value == 0) {
                    field_order_id.hide();
                    field_sku.hide();
                    field_email.hide();
                }
                else {
                    field_order_id.show();
                    field_sku.show();
                    field_email.show();
                }
            });
        }
    });
 
 });