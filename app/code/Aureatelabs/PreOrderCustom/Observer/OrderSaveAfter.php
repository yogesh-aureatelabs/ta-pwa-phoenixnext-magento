<?php
namespace Aureatelabs\PreOrderCustom\Observer;

class OrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct()
    {
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $status = $order->getStatus();
        $payment = $order->getPayment();
        $method = $payment->getMethod();

        if ($order->getInvoiceCollection()->count() && $status == "Pending_2C2P" && !$order->getShipmentsCollection()->count()) {
            $items = $order->getAllItems();
            $isPreorder = '';
            foreach ($items as $item) {
                $isPreorder = $item->getProduct()->getStockStatus();
                if ($isPreorder === 'PreOrder'){
                    break;
                }
            }
            if( $isPreorder === 'PreOrder'){
                $orderState = "pre_order";
                $orderStatus = "pre_ordered";
                $order->setState($orderState)->setStatus($orderStatus);
                $order->save();
            }
        }

        if ($status == "processing" && $status != "complete"){
            $items = $order->getAllItems();
            $isPreorder = '';
            foreach ($items as $item) {
                $isPreorder = $item->getProduct()->getStockStatus();
                if ($isPreorder === 'PreOrder'){
                    break;
                }
            }
            if( $isPreorder === 'PreOrder'){
                $orderState = "pre_order";
                $orderStatus = "pre_ordered";
                $order->setState($orderState)->setStatus($orderStatus);
                $order->save();
            }
        }
        if ( $method == "free" ) {
            $orderState = "paid_by_points";
            $order->setState($orderState)->setStatus($orderState);
            $order->save();
        }
    }
}
