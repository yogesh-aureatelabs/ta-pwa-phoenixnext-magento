<?php

namespace Aureatelabs\Base\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductSaveBefore implements ObserverInterface
{

    const DIR_PATH = '/var/www/www.phoenixnext.com/var/log/products/';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * RedirectToLive constructor.
     * @param \Magento\Framework\App\ResponseInterface $response
     * @param \Magento\Framework\UrlInterface $urlInterface
     */
    public function __construct(
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\App\State $state
    ) {
        $this->response = $response;
        $this->urlInterface = $urlInterface;
        $this->state = $state;
    }
    
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            $product = $observer->getProduct();
            $sku = $product->getSku();
            $date = date('Y-m-d H:i:s');
            $status = $product->getStockStatus();
            $code = $this->state->getAreaCode();

            $file = fopen(self::DIR_PATH . "$sku.txt", "a+");
            $txt  = "-----------------------------------------------\n";
            $txt .= "Type :: Before\n";
            $txt .= "SKU  :: $sku\n";
            $txt .= "Date :: $date\n";
            $txt .= "SKU  :: $status\n";
            $txt .= "Code :: $code\n";
            $txt .= "-----------------------------------------------\n";
            fwrite($file, $txt);
            fclose($file);
        } catch (\Exception $e) {
            return false;
        }
    }
}
