<?php

namespace Aureatelabs\Base\App\Router;

class NoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlInterface;

    /**
     * NoRouteHandler constructor.
     * @param \Magento\Framework\UrlInterface $urlInterface
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        $this->urlInterface = $urlInterface;
    }
    public function process(\Magento\Framework\App\RequestInterface $request)
    {
        header("location: https://phoenixnext.com",  true,  301);
        exit;
    }
}
