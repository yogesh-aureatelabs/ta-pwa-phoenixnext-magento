<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_HomeBanner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\HomeBanner\Model;

use Aureatelabs\HomeBanner\Api\Data\BannerInterface;

/**
 * Class Banner
 * @package Aureatelabs\HomeBanner\Model
 */
class Banner extends \Magento\Framework\Model\AbstractModel implements BannerInterface
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'aureate_home_banner';

    /**
     * @var string
     */
    protected $_cacheTag = 'aureate_home_banner';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'aureate_home_banner';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aureatelabs\HomeBanner\Model\ResourceModel\Banner');
    }

    /**
     * Get EntityId.
     *
     * @return int|mixed
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set EntityId.
     *
     * @param int $entityId
     * @return Banner|\Magento\Framework\Model\AbstractModel
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Title.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set Title.
     *
     * @param $title
     * @return Banner
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get Link.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getLink()
    {
        return $this->getData(self::LINK);
    }

    /**
     * Set Link.
     *
     * @param $link
     * @return Banner
     */
    public function setLink($link)
    {
        return $this->setData(self::LINK, $link);
    }

    /**
     * Get Image.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * Set Image.
     *
     * @param $image
     * @return Banner
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Get Active Status.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set Active Status.
     *
     * @param $isActive
     * @return Banner
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Get Sort order.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * Set Sort order.
     *
     * @param $sortOrder
     * @return Banner
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Get Creation Time.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set Creation Time.
     *
     * @param $createdAt
     * @return Banner
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get Modification Time.
     *
     * @return \Aureatelabs\HomeBanner\Api\Data\varchar|mixed
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATE_AT);
    }

    /**
     * Set Modification Time.
     *
     * @param $updatedAt
     * @return Banner
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATE_AT, $updatedAt);
    }
}
